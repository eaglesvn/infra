#!/usr/bin/env python

from bs4 import BeautifulSoup
import requests
import urllib3
import re
""" 
Not completed and require python3 for now to run it. 
"""

url = 'https://etqa4.eagletechva.com/dasis2/index.htm'
a_url = 'https://etqa4.eagletechva.com/dasis2/'

def return_soup(url):

  try:
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    urllib3.disable_warnings()
  except urllib3.exceptions.HTTPError as err:
    print(err)
  soup = BeautifulSoup(response.data, features="lxml")
  return soup

def get_html_links(b_soup):
  local_links = []
  outside_links = []
  no_match_index = re.compile(r'^(?!index\.*)')
  no_match_mail = re.compile(r'^(?!mail\.*)')
  match_http = re.compile(r'^http|^https')
  for link in b_soup.findAll('a'):
    #print(link.get('href'))
    if re.search(match_http, link.get('href')):
      outside_links.append(link.get('href'))
    else:
      if re.search(no_match_index, link.get('href')) and re.search(no_match_mail, link.get('href')):
        #print(link.get('href'))
        local_links.append(a_url + link.get('href'))
  return local_links, outside_links 

b_soup = return_soup(url)
l_links, o_links = get_html_links(b_soup)
other_htmls = []

def get_sub_links(rest_links):
  other_links = [] 
  for sub_link in rest_links:
    b_soup = return_soup(sub_link)
    sl_links, so_links = get_html_links(b_soup)
    for s_link in sl_links:
      if s_link not in l_links:
        if re.search('\w\.htm|\w\.html', s_link):
          print(s_link)
          other_links.append(s_link)
          l_links.append(s_link)
  if other_htmls:
    return other_links
  else:
    return 

my_lists = get_sub_links(l_links)
while True:
  if my_lists:
     print(my_lists)
     my_lists = get_sub_links(my_lists)
     print('I am here')
  else:
    print('I have got the none')
    break


def get_html_status(html_links):

  for html_link in html_links:
    reqs = requests.head(html_link, verify=False)
    print(html_link, reqs.status_code)
    print('********************')
    print()
    b_soup = return_soup(html_link)
    inner_links, outer_links = get_html_links(b_soup)
    for inner_link in inner_links:
      if inner_link not in html_links:
        reqs = requests.head(inner_link, verify=False)
        print(inner_link, reqs.status_code)


get_html_status(l_links)


