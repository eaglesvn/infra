#!/usr/bin/env python3

from bs4 import BeautifulSoup
import requests
import urllib3
import re
import getpass
import time
from requests.auth import HTTPBasicAuth
import os
import json
from json2html import *
from requests.exceptions import Timeout
import argparse
from subprocess import Popen, PIPE
import logging
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import csv
import datetime
import urllib.parse
from selenium import webdriver
import types 
logging.basicConfig(filename='/tmp/web_link_data_validation.log', filemode='w', format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

##defining the parser arguments
parser = argparse.ArgumentParser(description='Pass the required arguments')
parser.add_argument('-p', '--project', help='doc_site or dasis_home as the project', required=True)
parser.add_argument('-e', '--email_addresses', help='Enter the email addresses with command separated')
parser.add_argument('-s', '--save_files_dir', help='Enter the directory path where you want to save the files')

args = parser.parse_args()

if args.project:
    if args.project == 'doc_site' or args.project == 'dasis_home' or args.project == 'locator':
      project = args.project
    else:
      logging.CRITICAL('Pass the project argument as doc_site or dasis_home only')
      raise Exception('Pass the project argument as doc_site or dasis_home only')

if args.save_files_dir:
    save_files_dir = args.save_files_dir
else:
    save_files_dir = '.'

if args.email_addresses:
    toaddrs = args.email_addresses.replace(" ", "").split(',')

else:
    toaddrs = ['pavan.kumar@eagletechva.com']

fromaddr = "pavan.kumar@eagletechva.com"

if project == 'doc_site':
  login_url = 'https://etqa9.eagletechva.com/doc/default.htm'
  a_url = 'https://etqa9.eagletechva.com/doc/'
  #home_url = 'https://etqa9.eagletechva.com/docsite/callback'
  #a_url = 'https://etqa9.eagletechva.com/docsite/docProxy/'
  #login_url = 'https://etqa9.eagletechva.com/docsite'
  username = input('Enter the user name: ')
  password = getpass.getpass(prompt='Enter the password: ')
  login_data = dict()
  login_data['username'] = username
  login_data['password'] = password

elif project == 'dasis_home':
  a_url = 'https://etqa4.eagletechva.com/dasis2/'
  login_url = 'https://etqa4.eagletechva.com/dasis2/index.htm'
  login_data = None

elif project == 'locator':
  a_url = 'https://ettst21.eagletechva.com'
  login_url = 'https://ettst21.eagletechva.com'
  login_data = None
else:
  logging.ERROR('Pass the project argument as doc_site or dasis_home only')
  raise Exception('Pass the project argument as doc_site or dasis_home only')

urllib3.disable_warnings()
headers = { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0' }
web_data = dict()
all_links = set()
status_200 = set()
status_not_200 = set()
main_outside_links = []
out_links_status = dict()

##Send the error message to Email address
def send_messages(msg):
  html = ('<p><strong>{}</strong></p>'.format(msg))
  msg = MIMEMultipart()
  msg = MIMEText(html, "html")
  msg['From'] = fromaddr
  msg['To'] = ", ".join(toaddrs)
  msg['Subject'] = "ERROR: Not able to get the Release Version number for Application{}".format(component)

  server = smtplib.SMTP('eagletechva-com.mail.protection.outlook.com')
  server.sendmail(fromaddr, toaddrs, msg.as_string())
  server.quit()


## Possible of my first class 
#def get_soup_and_session(login_url, home_url, login_data):
#  ''' Return the home page link and session takes three arugment, login_url, home_url, login_data. '''

#  with requests.Session() as req_session:
#    login_page = req_session.get(login_url, headers=headers, verify=False)
#    login_soup = BeautifulSoup(login_page.content, 'html5lib')
#    csrf_token = login_soup.find('input', attrs= { 'name': 'csrfToken' })['value']
#    login_data['csrfToken'] = csrf_token
#    login_post = req_session.post(home_url, headers=headers, verify=False, data=login_data)
#    home_page = BeautifulSoup(login_post.content, 'html5lib')
#    #print(home_page)
#    return home_page, req_session


def get_soup_and_session(login_url, login_data=None):
  ''' Return the home page link and session takes three arugment, login_url, login_data. '''
  with requests.Session() as req_session:
    if login_data:
      req_session.auth = (login_data['username'], login_data['password'])
      web_page = req_session.get(login_url, headers=headers, verify=False)
      web_soup = BeautifulSoup(web_page.content, features="lxml")
      return web_soup, req_session
    else:
      web_page = req_session.get(login_url, headers=headers, verify=False)
      web_soup = BeautifulSoup(web_page.content, features="lxml")
      return web_soup, req_session


def get_html_links(b_soup, session, relative_path=None, page_link=None):
  hash_in_end = re.compile(r'#.*$')
  local_links = set()
  outside_links = set()
  match_hash_img = re.compile(r'img|^mail\.*|\.\.|#top$')
  match_http = re.compile(r'http|https|www')
  #match_hash_img = re.compile(r'\.htm#|\.html#|img|^mail\.*|\.\.|#.*$')
  if project == 'locator':
    if page_link:
      onclick_links = get_onclick_links_using_selenium(page_link)
      if onclick_links:
        a_tags = b_soup.findAll('a') + b_soup.findAll('A') + onclick_links
      else:
        a_tags = b_soup.findAll('a') + b_soup.findAll('A')
  else:
    a_tags = b_soup.findAll('a') + b_soup.findAll('A')

  for link in a_tags:
    if type(link) is not str:
      link_fmt = link.get('href')
      link_str = str(link_fmt)
    else:
      link_fmt = link
    if re.search(match_hash_img, link_str) or link_fmt == None:
      continue
    try:
      if re.search(match_http, link_fmt):
        if re.search(hash_in_end, link_fmt):
          out_link_without_hash = re.sub(hash_in_end, '',link_fmt)
          if out_link_without_hash:
            outside_links.add(out_link_without_hash)
        else:
          outside_links.add(link_fmt)
      else:
        if relative_path:
          link_path = a_url + relative_path + '/' + link_fmt
          link_header = session.head(link_path, headers=headers, verify=False)
          link_data_type = link_header.headers['Content-Type']
          if 'text/html' in link_data_type:
            if re.search(hash_in_end, link_path):
              link_without_hash = re.sub(hash_in_end, '',link_path )             
              local_links.add(link_without_hash)
            else:
              local_links.add(link_path)      
          else:
            local_links.add(link_path)
        else:
          link_path = (a_url + link_fmt)
          link_header = session.head(link_path, headers=headers, verify=False)
          link_data_type = link_header.headers['Content-Type']
          if 'text/html' in link_data_type:
            if re.search(hash_in_end, link_path):
              link_without_hash = re.sub(hash_in_end, '',link_path )  
              local_links.add(link_without_hash)
            else:
              local_links.add(link_path)        

          else:
            local_links.add(link_path)
    except Exception as e:
      pass
  
  local_links = list(local_links)
  outside_links = list(outside_links)
  return local_links, outside_links

def get_sub_links(rest_links):
  other_links = []
  for rest_link in rest_links:
    reqs = session.head(rest_link, headers=headers, verify=False)
    link_data_type = reqs.headers['Content-Type']
    if 'text/html' not in link_data_type:
      continue
    else:
      sub_path = rest_link.replace(a_url, '')
      paths = sub_path.split('/')[:-1]
      if len(paths) >= 1:
        if '..' not in paths:
          if project == 'doc_site':
            relative_path = "/".join(paths)
          else:
            relative_path = None
          sub_link = session.get(rest_link, headers=headers, verify=False)
          sub_soup = BeautifulSoup(sub_link.content, features="lxml")
          sl_links, _ = get_html_links(sub_soup, session, relative_path=relative_path, page_link=rest_link)
      else:
        sub_link = session.get(rest_link, headers=headers, verify=False)
        sub_soup = BeautifulSoup(sub_link.content, features="lxml")
        sl_links, _ = get_html_links(sub_soup, session, page_link=rest_link)
    for s_link in sl_links:
      if s_link not in in_links:
        reqs = session.head(s_link, headers=headers, verify=False)
        link_data_type = reqs.headers['Content-Type']
        if 'text/html' in link_data_type:
         other_links.append(s_link)
         in_links.append(s_link)
  if other_links:
    return other_links
  else:
    return 

def link_data_format(reqs, link):
  link = urllib.parse.unquote(link)
  data_status = None
  if 'Content-Length' in reqs.headers:
    data_size = reqs.headers['Content-Length']
  else:
    data_size = 'No Length'
  if 'Last-Modified' in reqs.headers:
    last_modified_date = reqs.headers['Last-Modified']
  else:
    last_modified_date = 'No Date'
  if reqs.status_code == 200:
    status_200.add(link + ' - - ' + str(reqs.status_code) + ' - - ' + str(data_size) + ' - - ' + str(last_modified_date))
    data_status = (link + ' - - ' + str(reqs.status_code) + ' - - ' + str(data_size) + ' - - ' + str(last_modified_date))
  else:
    status_not_200.add(link + ' - - ' + str(reqs.status_code) + ' - - ' + str(data_size) + ' - - ' + str(last_modified_date))
    data_status = (link + ' - - ' + str(reqs.status_code) + ' - - ' + str(data_size) + ' - - ' + str(last_modified_date))
  return data_status

def out_link_data_format(out_link):
  out_link_data_status = None
  out_link = out_link.rstrip('\n')
  try:
    reqs = requests.head(out_link, headers=headers, verify=False, timeout=(2, 5))
  except Timeout:
    print('Request got timeout', out_link)
    out_link_data_status = (out_link + ' - - ' + 'TimeOut')
  except Exception as e:
    print('ERROR with Link: ', out_link)
    out_link_data_status = (out_link + ' - - ' + 'Response Error')
  else:
    if 'Content-Length' in reqs.headers:
      data_size = reqs.headers['Content-Length']
    else:
      data_size = 'No Length'
    if 'Last-Modified' in reqs.headers:
      last_modified_date = reqs.headers['Last-Modified']
    else:
      last_modified_date = 'No Date'
    out_link = urllib.parse.unquote(out_link)
    if reqs.status_code:
      status_200.add(out_link + ' - - ' + str(reqs.status_code) + ' - - ' + str(data_size) + ' - - ' + str(last_modified_date))
      out_link_data_status = (out_link + ' - - ' + str(reqs.status_code) + ' - - ' + str(data_size) + ' - - ' + str(last_modified_date))
    #else:
      if reqs.status_code not in range(200,399):
        print('Checking the status: ', reqs.status_code)
        status_not_200.add(out_link + ' - - ' + str(reqs.status_code) + ' - - ' + str(data_size) + ' - - ' + str(last_modified_date))
      #out_link_data_status = (out_link + ' - - ' + str(reqs.status_code) + ' - - ' + str(data_size) + ' - - ' + str(last_modified_date))
  return out_link_data_status



def get_html_status(html_links):
  for html_link in html_links:
    sub_links = []
    out_links = []
    _, session = get_soup_and_session(login_url, login_data=login_data)
    reqs = session.head(html_link, headers=headers, verify=False)
    print(html_link, reqs.status_code)
    all_links.add(html_link)
    head_link = link_data_format(reqs, html_link)
    link_data_type = reqs.headers['Content-Type']
    if 'text/html' in link_data_type:
      b_soup, session = get_soup_and_session(html_link,  login_data=login_data)
      sub_path = html_link.replace(a_url, '')
      paths = sub_path.split('/')[:-1]
      if len(paths) >= 1:
        if project == 'doc_site':
          relative_path = "/".join(paths)
        else:
          relative_path = None
        inner_links, outter_links = get_html_links(b_soup, session, relative_path=relative_path, page_link=html_link)
      else:
        inner_links, outter_links = get_html_links(b_soup, session, page_link=html_link)
    else:
      reqs = session.head(html_link, headers=headers, verify=False)
      link_data_format(reqs, html_link)
      continue
    for inner_link in inner_links:
      if project == 'locator':
        #if inner_link not in html_links:
          all_links.add(inner_link)
          reqs = session.head(inner_link, headers=headers, verify=False)
          sub_link = link_data_format(reqs, inner_link)
          sub_links.append(sub_link)
      else:
        if inner_link not in html_links:
          all_links.add(inner_link)
          reqs = session.head(inner_link, headers=headers, verify=False)
          sub_link = link_data_format(reqs, inner_link)
          sub_links.append(sub_link)

    for outer_link in outter_links:
      out_link = out_link_data_format(outer_link)
      out_links.append(out_link)
     
    web_data[head_link] = (sub_links + out_links)
    main_outside_links.extend(outter_links)
  return all_links

def outside_links_status(outside_links):
  out_links = set(outside_links)
  out_links_values = []
  out_links_status = dict()
  for out_link in out_links:
    out_link = out_link.rstrip('\n')
    try:
      reqs = requests.head(out_link, headers=headers, verify=False, timeout=(2, 5))
    except Timeout:
      print('Request got timeout', out_link)
      out_links_values.append(out_link + ' - - ' + 'TimeOut')
      status_not_200.add(out_link + ' - - ' + 'TimeOut')
    except Exception as e:
      print('ERROR with Link: ', out_link)
      out_links_values.append(out_link + ' - - ' + 'Has Other Error')
      status_not_200.add(out_link + ' - - ' + 'Has Other Error')
    else:
      print(reqs.status_code, out_link)
      out_links_values.append(out_link + ' - - ' + str(reqs.status_code))
      if reqs.status_code not in range(200,399):
        print('Checking the status: ', reqs.status_code)
        status_not_200.add(out_link + ' - - ' + str(reqs.status_code))
  out_links_status['Outer Links'] = out_links_values
  return out_links_status

def create_csv_data_file(csv_file, data):
  with open(csv_file, mode='w') as data_file:
    data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    data_writer.writerow(['parent', 'link', 'Status', 'Data Size', 'Date Modified'])
    for key, value in data.items():
      key_v = key.split(' - - ')
      key_v.insert(1, '')
      data_writer.writerow(key_v)
      #parent = key_v[1]     
      for val in value:
        value = val.split(' - - ')
        value.insert(0, key_v[0])
        data_writer.writerow(value)
      data_writer.writerow([])

def create_csv_of_fail_links(csv_file, data):
  with open(csv_file, mode='w') as data_file:
    data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    data_writer.writerow(['link', 'Status'])
    for  val in data:
        value = val.split(' - - ')
        data_writer.writerow(value)


#Function to catch the onclick html files using beautiful soup, it is not in use. It works fine and faster. But selenium function in use as per demand.
def get_onclick_links_using_bs(b_soup):
  onclick_pattern = re.compile('onclick\=\"openLocation')
  li_tags = b_soup.find('body')
  for li_tag in li_tags:
    if re.search(onclick_pattern, str(li_tag)):
      html_pages = re.findall('\/locator.*\.html', str(li_tag))
      if html_pages:
        return html_pages

def get_onclick_links_using_selenium(page_link):
  all_links = []
  options = webdriver.ChromeOptions()
  options.add_argument('--ignore-certificate-errors')
  options.add_argument('--headless')
  options.add_argument('--no-sandbox')
  options.add_argument('--disable-gpu')
  driver = webdriver.Chrome('./driver/chromedriver', options=options)
  driver.get(page_link) 
  link_source = driver.page_source
  driver.close()
  onclick_all_pattern = re.compile('onclick\=\"openLocation.*\)')
  onclick_links = re.findall(onclick_all_pattern, link_source)
  if onclick_links:
    for onclick_link in onclick_links:
      link_find = re.search('(\/.*)(?=\'\))', onclick_link)
      all_links.append(link_find.group())
  if all_links:
    return all_links
    





##
'''
Calling Main Functions
'''

b_soup, session = get_soup_and_session(login_url,  login_data=login_data)
  
in_links, out_links = get_html_links(b_soup, session, page_link=login_url)

in_sub_links = get_sub_links(in_links)

while True:
  if in_sub_links:
     in_sub_links = get_sub_links(in_sub_links)
  else:
    print('No more sub links are availble, coming out from the while loop, and finishing remaining functions.')
    break

if project == 'doc_site' or project == 'dasis_home' or project == 'locator':
  inner_title = dict()
  print(in_links)
  all_html_data = get_html_status(in_links)
  all_outside_links = list(set(main_outside_links))
  outer_htmls_status = outside_links_status(all_outside_links)
  #total_data = {**outer_htmls_status, **web_data}
  total_data = {**web_data}
  #total_data = outer_htmls_status + list(status_not_200) + list(status_200)
  failed_status = list(status_not_200)
  file_date = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
  csv_file = ('data/' + project + '_html_data_' + file_date + '.csv')
  all_data = ('data/' + project + '_full_links_data.txt')
  json_data = ('data/' + project + '_html_data_' + file_date + '.json')
  fail_links = ('data/' + project + '_fail_links' + file_date + '.csv')
  html_convert_data = ('data/' + project + '_html_data_' + file_date + '.html')

  create_csv_data_file(csv_file, total_data)
  
  create_csv_of_fail_links(fail_links, failed_status)

  with open(json_data, 'w') as j_d:
    json.dump(web_data, j_d)

  if os.path.exists(all_data):
    os.remove(all_data)

  with open(all_data, 'a') as a_d:
    for h_d in sorted(all_html_data):
      unquote_link = urllib.parse.unquote(h_d)
      a_d.write('{}\n'.format(unquote_link))

  if os.path.exists(json_data):
    with open(json_data, 'r') as j_l:
      json_load_data = json.load(j_l)
  html_data_from_json = json2html.convert(json = json_load_data)

  if os.path.exists('template_start.html') and os.path.exists('template_end.html') :
    with open('template_start.html', 'r') as s_h:
      start_html = s_h.read()
    with open('template_end.html', 'r') as e_h:
      end_html = e_h.read()

  converted_html_data = start_html + end_html.format(html_data_from_json)
  with open(html_convert_data, 'w') as c_h:
    c_h.write(converted_html_data)

else:
  raise Exception('Project name is not in the list')

