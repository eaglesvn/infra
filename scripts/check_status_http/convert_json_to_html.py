import json
from json2html import *


with open('doc_files.json', 'r') as jf:
  datastor = json.load(jf)

my_html = json2html.convert(json = datastor)

with open('doc_file_template_start.html', 'r') as tr:
  start_html = tr.read()

with open('doc_file_template_end.html', 'r') as tr:
  end_html = tr.read()

doc_html =  start_html + end_html.format(my_html)

print(doc_html)
with open('doc_file.html', 'w') as fw:
  fw.write(doc_html)



