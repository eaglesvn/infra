#!/usr/bin/env python3
'''
Created by Pavan Kumar
Purpose of the script is to get the doc-stie and dasis_home page files
'''
import re
import argparse
import os
from pexpect import pxssh
import getpass
import csv

##defining the parser arguments
parser = argparse.ArgumentParser(description='Pass the required arguments')
parser.add_argument('-p', '--project', help='doc_site or dasis_home as the project', required=True)
parser.add_argument('-e', '--email_addresses', help='Enter the email addresses with command separated')
parser.add_argument('-s', '--save_files_dir', help='Enter the directory path where you want to save the files')

args = parser.parse_args()

if args.project:
    if args.project == 'doc_site' or args.project == 'dasis_home':
      project = args.project
    else:
      logging.CRITICAL('Pass the project argument as doc_site or dasis_home only')
      raise Exception('Pass the project argument as doc_site or dasis_home only')

if args.save_files_dir:
    save_files_dir = args.save_files_dir
else:
    save_files_dir = '.'

if args.email_addresses:
    toaddrs = args.email_addresses.replace(" ", "").split(',')

else:
    toaddrs = ['pavan.kumar@eagletechva.com']

fromaddr = "pavan.kumar@eagletechva.com"

doc_site_host = 'etqa4web01'
dasis_home_host = 'etqa4web02'
doc_site_dir = '/var/www/html/doc'
dasis_home_dir = '/var/www/html/dasis2'
doc_site_URL = 'https://etqa9.eagletechva.com/doc'
dasis_home_URL = 'https://etqa4.eagletechva.com/dasis2'


username = input('unix username: ')
password = getpass.getpass('unix password: ')

def get_file_stats(username, password, hostname, data_file):
  data_stats = []
  try:
    s = pxssh.pxssh()
    s.login(hostname, username, password)
    for d_f in data_file:
      command = ("stat -c  %s--%z " + d_f +" | awk -F'.' '{print $1}'")
      s.sendline(command)
      s.prompt()
      data_line = s.before.decode()
      data_size_time = data_line.split('\n')[1].strip('\r')
      data_arrange =  d_f + '--' + data_size_time
      data_stats.append(data_arrange)
    s.logout()
  except pxssh.ExceptionPxssh as e:
    print("pxssh failed on login.")
    print(e)
  return data_stats

if project == 'doc_site':
  data_file = []
  read_file = ('data/' + project + '_difference_in_data.txt')
  with open(read_file, 'r') as d_f:
    d_file = d_f.readlines()
  for d_l in d_file:
    sub_line = re.sub(doc_site_URL, doc_site_dir, d_l)
    sub_line = sub_line.strip('\n')
    data_file.append('"' + sub_line + '"')
 
  stats_data = get_file_stats(username, password, doc_site_host, data_file)
  stats_file = ('data/' + project + '_files_stats.csv')
  with open(stats_file, mode='w') as d_w:
    data_writer = csv.writer(d_w, delimiter=',')
    data_writer.writerow(['HTML Page', 'Size', 'Date'])
    for s_d in stats_data[1:]:
      print(s_d)  
      data_writer.writerow(s_d.split('--'))

elif project == 'dasis_home':
  data_file = []
  read_file = ('data/' + project + '_difference_in_data.txt')
  with open(read_file, 'r') as d_f:
    d_file = d_f.readlines()
  for d_l in d_file:
    sub_line = re.sub(dasis_home_URL, dasis_home_dir, d_l)
    sub_line = sub_line.strip('\n')
    data_file.append('"' + sub_line + '"')
 
  stats_data = get_file_stats(username, password, dasis_home_host, data_file)
  stats_file = ('data/' + project + '_files_stats.csv')     
  with open(stats_file, mode='w') as d_w:
    data_writer = csv.writer(d_w, delimiter=',')
    data_writer.writerow(['HTML Page', 'Size', 'Date'])
    for s_d in stats_data[1:]:
      data_writer.writerow(s_d.split('--'))

else:
  raise Exception('You have not selceted the project')
