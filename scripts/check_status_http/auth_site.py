#!/usr/bin/env python

from bs4 import BeautifulSoup
import requests
import urllib3
import re
import getpass

url = 'https://etqa9.eagletechva.com/docsite'
#a_url = 'https://etqa4.eagletechva.com/dasis2/'


login_data = dict()

username = input('Enter the user name: ')
password = getpass.getpass(prompt='Enter the password: ')

login_data['username'] = username
login_data['password'] = password




headers = { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0' }

with requests.Session() as s:
  req = s.get(url, headers=headers, verify=False)
  soup = BeautifulSoup(req.content, 'html5lib')
  find_csrf = soup.find('input', attrs= { 'name': 'csrfToken' })['value']
  login_data['csrfToken'] = find_csrf
  ps = s.post('https://etqa9.eagletechva.com/docsite/callback', headers=headers, verify=False, data=login_data)
  inside_login = BeautifulSoup(ps.content, 'html5lib')
  print(inside_login)


