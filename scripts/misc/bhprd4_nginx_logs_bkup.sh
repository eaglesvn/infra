#!/bin/bash
##Created By Pavan Kumar
##This script copies the nginx access log files of locator to backup server 

ID="ansible"
PRX_HOSTNAME="bhprd4.eagletechva.com"
NGX_LOG_REMOTE_PATH="/var/log/nginx/access.*"
DATE=$(date +%F-%T)
LOG_FILE=/tmp/bhprd4_ngx_access_rsync_${DATE}.log
NGX_LOG_LOCAL_PATH="/local/apps/bhsis/bhprd4_nginx_logs"
EMAIL_ID=pavan.kumar@eagletechva.com

function get_nginx_log_files {

rsync --ignore-existing  -r -a -v -e ssh ${ID}@${PRX_HOSTNAME}:${NGX_LOG_REMOTE_PATH} ${NGX_LOG_LOCAL_PATH} --log-file=${LOG_FILE}



}

function checking_the_error_on_log_file {

FOUND_ERROR=$(grep -Ei "error| failed | fail" ${LOG_FILE})

if [[ ${FOUND_ERROR} != "" ]]
then

echo "BHPRD4 Nginx Log Access rsync is Failed: Please check" | mailx -s "FAILED: bhprd4 Nginx log copy rsync to backup server is Failed" pavan.kumar@eagletechva.com 


else 

echo "BHPRD4 Nginx Log Access rsync is completed" | mailx -s "SUCCESS: bhprd4 Nginx log copy rsync to backup server is completed" pavan.kumar@eagletechva.com
fi

}

get_nginx_log_files

checking_the_error_on_log_file
