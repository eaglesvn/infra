#!/usr/bin/env python
import re, sys, os, subprocess
import shutil 
'''This script remove the robots IP addresses and there entry from the dasis
home page nginx log file.'''


if len(sys.argv) == 2:
    log_file = sys.argv[1]
    if not os.path.exists(log_file):
        sys.exit('Argument log file does not exist {}'.format(log_file))
else:
    sys.exit('Pass the log file as a first argument to the script only')

#filter_log_file = re.split('\.',os.path.basename(log_file))[0]
#pat = re.split('_', pat)[-1]
 
#print pat



def log_access_file(log_file):
    try:
        os.path.exists(log_file)
        with open(log_file, 'r') as lg_rh: #lg_rh means log file read handler
            log_file_lines = lg_rh.readlines()
        return log_file_lines
    except IOError as e:
        sys.exit('Log file: {} does not exist'.format(e))

log_file_lines = log_access_file(log_file)

def get_eagle_network_ips():
    '''Getting the list of Eagle network IPs from the log file.
    Eagle network IPs use by the Eagle internal team and its business team. '''  
    internal_ips = []
    ip_matches = ['^(38\.100\.23\.[0-9]{1,3})',
	'^(38\.105\.76\.[0-9]{1,3})',
	'^(50\.206\.81\.[64-71])',
	'^(50\.206\.81\.[96-99]|50\.206.81\.[100-127])',
	'^(173\.166\.149\.1[6-9]|173\.166\.149\.1[20-23])',
	'^(12\.127\.249\.18[8-9]|12\.127\.249\.19[0-1])',
	'^12\.203\.158\.[0-9]{1,3}',
	'^172\.28\.[0-9]{1,3}\.[0-9]{1,3}',
	'^172\.29\.[0-9]{1,3}\.[0-9]{1,3}', 
	'^10\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}']

    for log_file_line in log_file_lines:
        for ip_match in ip_matches:
            find_ip = re.search(ip_match, log_file_line)
            if find_ip is not None:
                internal_ips.append(log_file_line.split(' - -')[0])
    internal_ips = list(set(internal_ips))
    return internal_ips

def matches_sc_frg_nessus():
    ''' Getting the Scream frog and Nessus name from the log file. 
    Return the names of Screaming Frog and Nessus in to the list. '''

    matches = re.compile(r'screaming frog| Nessus', re.IGNORECASE)
    sc_frg_nses = []
    for log_file_line in log_file_lines:
        find = re.search(matches, log_file_line)
        if find is not None:
            sc_frg_nses.append(find.group())
    sc_frg_nses = list(set(sc_frg_nses))
    return sc_frg_nses

def match_bots():
    ''' Getting all the bots entries from the log file 
    and return the bot names. '''

    matches = re.compile(r'\w+[\W\s]?bot', re.IGNORECASE)
    bots = []
    for log_file_line in log_file_lines:
        find = re.search(matches, log_file_line)
        if find is not None:
            bots.append(find.group())
    bots = list(set(bots))
    return bots

def filtered_log(log_file):
    '''Generate the filter log file in the end by using the above methods return.'''

    shutil.copyfile(log_file, 'tmp_read_log_file')
    read_log_file = 'tmp_read_log_file'
    write_log_file = ('filtered_'+log_file)
    filters = get_eagle_network_ips() + matches_sc_frg_nessus() + match_bots()
    for filter in filters:
        if os.path.exists(write_log_file):
            os.remove(write_log_file)
        with open(read_log_file) as fa, open(write_log_file, 'a') as fw:
            for line in fa:
                if filter not in line:
                    fw.write(line)
        shutil.copyfile(write_log_file, read_log_file)

filtered_log()

