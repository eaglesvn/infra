#!/bin/python
##Created by Pavan Kumar
##This script run the hits_on_homepage.sh script and make one access.log file of a week
## I will add exceptions later

import os
import sys, paramiko
from datetime import timedelta, datetime, tzinfo, date
import getpass, re, subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--days', type=int, help="add the -d or --day argument with number of days")
#parser.parse_args()
args =  parser.parse_args()
print args.days



today_date = date.today()
html_file_loc = '/local/apps/bhsis/hit_htmls'
hit_count_script = '/local/apps/bhsis/scripts/hits_on_homepage.sh'
html_file = "%s/hits_count_%s.html" % (html_file_loc, today_date)
combine_access_log_file = "%s/combine_access_%s.log" % (html_file_loc, today_date)
print(html_file)
if (os.path.isfile(html_file)) or (os.path.isfile(combine_access_log_file)):
	subprocess.call("rm -f %s" % (html_file), shell=True)
	subprocess.call("rm -f %s" % (combine_access_log_file), shell=True)
else:
	pass


week_days = args.days
path = '/local/apps/bhsis/hit_htmls/access_files'
for week_day in range(1, week_days ):
	date_with_dash = date.today() - timedelta(week_day)
	date_without_dash = re.sub('-', '', str(date_with_dash))
	print "date is now %s" % (date_without_dash)
	access_zip_file = "access.log-%s.gz"  % (date_without_dash)
	file_with_path = "%s/%s" % (path, access_zip_file)
	#print (file_with_path)
	if os.path.isfile(file_with_path):
		print "File name %s exist" % (file_with_path)
		subprocess.call("zcat %s >> %s" % (file_with_path, combine_access_log_file), shell=True)
	else:
		print("File does not exit")
	

	


subprocess.call("/bin/bash %s %s > %s" % (hit_count_script, combine_access_log_file, html_file), shell=True)

