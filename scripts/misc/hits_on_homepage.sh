#!/bin/sh
##Created by Pavan Kumar

DATE_MONTH=$(date +%b)
WEEK_DATE=$(echo "$(date -dlast-wednesday +%d) - $(date +%d)")

#ACCESS_LOG_FILE=/var/log/nginx/access.log
#ACCESS_LOG_FILE=/var/log/nginx/access.log_092817
HITS_ON="hits on"

function usage () {

printf "Pass the nginx access log file as the parameter\n"

}

if [[ -z "$1" ]]

then
	usage
	exit 1

else
	if [[ -f $1 ]]
	then

		ACCESS_LOG_FILE=$1
	else
		printf "$1 file does not exist please check the file and it\'s location\n"
		exit 1
	fi

fi 


echo "

<!DOCTYPE html>
<html>
<head>
<style>


#p01 {
    text-decoration: underline

}

pre
    {
    display: inline;
    }


</style>
</head>
<body>
"

echo "Hits on DASIS Website" | tee -a
echo "BHSIS Web Hits ${DATE_MONTH} ${WEEK_DATE} are shown below." | tee -a



echo "" | tee -a

echo "<h3>TEDS</h3>"

echo "<pre>"

TOTAL_SYB=52
FIRST_STR="TEDS Manual"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"


TOTAL_SYB=50
FIRST_STR=" Instruction Manual V4.3 PDF"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "combined_su_mh_teds_manual_ver_4.3.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "Combined SA and MH TEDS State"
echo " $FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS "

TOTAL_SYB=50
FIRST_STR="DSS State User Manual V2.1"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "dss_manual_v2.1.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo " $FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS "


echo ""
TOTAL_SYB=52
FIRST_STR="TEDS Data/Reports"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"
YEAR=2016
TOTAL_SYB=50
FIRST_STR="${YEAR} Combined TEDS Annual Reports"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_teds_rpt_con.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo " $FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS "

#echo -e "$(/usr/bin/grep -i "Combined%20SA%20and%20MH%20TEDS%20Manual%20V4.2_6-1.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l) ${HITS_ON} the Combined SA and MH TEDS State Instruction Manual V4.2 PDF"
#echo -e  "$(/usr/bin/grep -i "DSS_Manual_v2_6-1.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l) ${HITS_ON} the DSS State User Manual V2.0" | tee -a
echo ""
TOTAL_SYB=52
FIRST_STR="TEDS Web Tables"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="TEDS Web Tables"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "tabYearDotChooseYearWebTable" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo " $FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS "



echo ""| tee -a
echo "TEDS National Admissions,"| tee -a

TOTAL_SYB=52
FIRST_STR="Highlights, and State Reports"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"


TOTAL_SYB=50

YEAR=1995
while [ ${YEAR} -le 2001 ];do

FIRST_STR="${YEAR} TEDS Admissions"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_teds_rpt.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR+1
done

YEAR=2002
while [ ${YEAR} -le 2007 ];do

FIRST_STR="${YEAR} TEDS Admissions"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_teds_rpt.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="${YEAR} TEDS Highlights"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_teds_highlights_rpt.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"
let YEAR=YEAR+1
done

YEAR=2008
while [ ${YEAR} -le 2015 ];do

FIRST_STR="${YEAR} TEDS National"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_teds_rpt_natl.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="${YEAR} TEDS State"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_teds_rpt_st.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR+1
done

echo ""

echo "Public Use Files - TEDS Admissions"
TOTAL_SYB=52
FIRST_STR="Codebook"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="2000-2016 Concatenated"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "TEDSA_00_16_CODEBOOK.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="DS1: 1992-1994"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "DS1_92_94_TEDSA_CODEBOOK.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


FIRST_STR="DS2: 1995-1999"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "DS2_95_99_TEDSA_CODEBOOK.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="DS3: 2000-2004"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "TEDSA_00_04_CODEBOOK.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


FIRST_STR="DS4: 2005-2009"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "TEDSA_05_09_CODEBOOK.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="DS5: 2010-2014"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "TEDSA_10_14_CODEBOOK.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="DS6: 2015-2016"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "TEDSA_15_16_CODEBOOK.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"



YEAR=2016
while [ 2000 -le ${YEAR} ];do
FIRST_STR="${YEAR} TEDS Admissions"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "TEDSA_${YEAR}_CODEBOOK.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR-1
done


YEAR=1999
while [ 1992 -le ${YEAR} ];do
FIRST_STR="${YEAR} TEDS Admissions"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_TEDSA_CODEBOOK.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR-1
done


echo ""

TOTAL_SYB=52
FIRST_STR="TEDS Discharge Reports"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
YEAR=2002
while [ ${YEAR} -le 2015 ];do
FIRST_STR="${YEAR} TEDS Discharge"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_teds_rpt_d.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR+1
done


echo ""| tee -a

echo "Public Use Files - TEDS Discharges"
TOTAL_SYB=52
FIRST_STR="Codebook"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="2006-2014 Concatenated"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "teds_d_2006_2014_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


FIRST_STR="2016 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "tedsd_2016_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"



FIRST_STR="2015 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "tedsd_2015_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"



FIRST_STR="2014 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "teds_d_2014_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2013 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "teds_d_2013_puf_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2012 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "teds_d_2012_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2011 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "35074-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2010 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "34898-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2009 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "33621-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2008 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "29901-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2007 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "27301-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2006 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "24461-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"
echo ""
TOTAL_SYB=52
FIRST_STR="SAS Format"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

FIRST_STR="2015 TEDS Discharges"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "tedsd_2015_puf_fmt.zip" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""
TOTAL_SYB=52
FIRST_STR="Most recent TEDS State Tables Link"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="Most recent TEDS State Tables Link"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "tables_mounted.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""

TOTAL_SYB=52
FIRST_STR="Crosswalks"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

echo "Combined Substance Abuse and"

TOTAL_SYB=50
FIRST_STR="Mental Health Crosswalk"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "Combined_SA-MH_Crosswalk_6-1.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="TEDS State Crosswalk"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "crosswalks.htm" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""| tee -a

echo "<h3>N-SSATS</h3>"
echo ""
TOTAL_SYB=52
FIRST_STR="UFDS Reports"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"
TOTAL_SYB=50
YEAR=1997
while [ ${YEAR} -le 1999 ];do
FIRST_STR="${YEAR} UFDS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_ufds_rpt.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR+1
done
echo ""
TOTAL_SYB=52
FIRST_STR="N-SSATS Reports"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
YEAR=2000
while [ ${YEAR} -le 2011 ];do

if [  ${YEAR} -eq 2001 ]

then
:

else


FIRST_STR="${YEAR} N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_nssats_rpt.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"
fi
let YEAR=YEAR+1

 
done


FIRST_STR="2011 (OTP) Survey"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "OTP2011_Web.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

YEAR=2012
while [ ${YEAR} -le 2017 ];do
FIRST_STR="${YEAR} N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_nssats_rpt.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR+1
done

echo ""
TOTAL_SYB=52
FIRST_STR="N-SSATS State Profiles"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="State Profiles"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "tabYearDotChooseYearStateProfile" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""| tee -a


echo ""| tee -a
 echo "National Survey of Substance Abuse Treatment Services (N-SSATS)"| tee -a
         echo "Survey Schedule"| tee -a

##Temporary not available need to add back when change will be there

#TOTAL_SYB=52
#FIRST_STR="Survey Schedule"
#END_STR=Hits
#LEN_FIRST_STR=$(echo ${#FIRST_STR})
#LEN_END_STR=$(echo ${#END_STR})
#ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
#NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
#echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

 
#TOTAL_SYB=50
#FIRST_STR="Most recent N-SSATS schedule"
#TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "N-SSATS%202016%20Schedule.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
#LEN_FIRST_STR=$(echo ${#FIRST_STR})
#LEN_END_STR=$(echo ${#TOTAL_HITS})
#ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
#NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
#echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"
echo ""
echo "Current State response rates"| tee -a

TOTAL_SYB=52
FIRST_STR="Current State response rates"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="Current State response rates"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "nssats_resp_rates.htm" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""
echo "<h3>I-BHS On-Line</h3>"| tee -a

TOTAL_SYB=52
FIRST_STR="I-BHS"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="I-BHS On-Line"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "IBHS_5.0_State_User_Manual_final.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""| tee -a
 echo "<h3>Questionnaires</h3>"| tee -a

TOTAL_SYB=52
FIRST_STR="UFDS"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="1997 UFDS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "97_ufds_quest.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


FIRST_STR="1998 UFDS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "98_ufds_quest.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="1999 UFDS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "99_ufds_quest.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""
TOTAL_SYB=52
FIRST_STR="N-SSATS"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
YEAR=2000
while [ ${YEAR} -le 2018 ];do

if [  ${YEAR} -eq 2001 ]

then
:

else

 
FIRST_STR="${YEAR} N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "nssats_${YEAR}_q.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

	if [  ${YEAR} -eq 2011 ]

	then
		FIRST_STR="${YEAR} OTP"
		TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "nssats_${YEAR}_OTP_q.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
		LEN_FIRST_STR=$(echo ${#FIRST_STR})
		LEN_END_STR=$(echo ${#TOTAL_HITS})
		ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
		NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
		echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


	else

		:

	fi 

fi 
let YEAR=YEAR+1

done

echo ""| tee -a
 echo "<h3>Directories</h3>"| tee -a

TOTAL_SYB=52
FIRST_STR="Substance Abuse"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"


TOTAL_SYB=50
YEAR=1975
FIRST_STR="1975 N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "directory_${YEAR}.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

YEAR=1979
FIRST_STR="1979 N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "directory_${YEAR}.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


YEAR=1980
FIRST_STR="1980 N-SSATS Alcohol"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "directory_${YEAR}_alcohol.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

YEAR=1980
FIRST_STR="1980 N-SSATS Drug"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "directory_${YEAR}_drug.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


TOTAL_SYB=50
YEAR=1982
while [ ${YEAR} -le 2018 ];do
FIRST_STR="${YEAR} N-SSATS"


if [  ${YEAR} -eq 1983 -o   ${YEAR} -eq 1985 -o  ${YEAR} -eq 1986 -o  ${YEAR} -eq 1988 -o  ${YEAR} -eq 1993 -o  ${YEAR} -eq 1999 -o  ${YEAR} -eq 2002 ]

then 
:

else

TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "directory_${YEAR}.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"
fi

let YEAR=YEAR+1
done

echo ""
TOTAL_SYB=52
FIRST_STR="Mental Health"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"


TOTAL_SYB=50

FIRST_STR="2008 Directory"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2008_nmhss_directory.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2008 Directory Index A"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2008_nmhss_directory_indx_a.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2008 Directory Index B"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2008_nmhss_directory_indx_b.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


YEAR=2012
while [ ${YEAR} -le 2018 ];do
if [[ ${YEAR} -eq 2013 ]] && [[ ${YEAR} -eq 2014 ]]
   then
       :
else
FIRST_STR="${YEAR} Directory"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_nmhss_directory.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

fi
let YEAR=YEAR+1
done



echo ""| tee -a
 echo "<h3>State Profiles</h3>"| tee -a
TOTAL_SYB=52
FIRST_STR="N-SSATS"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
YEAR=2002
while [ ${YEAR} -le 2017 ];do
FIRST_STR="${YEAR} N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "n${YEAR}_st_profiles.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR+1
done


echo ""| tee -a
echo "<h3>Public Use Files (PUFs)</h3>"

TOTAL_SYB=52
FIRST_STR="UFDS/N-SSATS"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50


FIRST_STR="2017 UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2017_nssats_puf_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"



FIRST_STR="2016 UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2016_nssats_puf_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


FIRST_STR="2015 UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2015_nssats_puf_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2014 UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2014_NSSATS_PUF_Codebook.PDF" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


YEAR=2013
while [ 2007 -le ${YEAR} ];do
FIRST_STR="${YEAR} UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_NSSATS_PUF_Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"
let YEAR=YEAR-1
done


FIRST_STR="2006 UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "20004-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2005 UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "04469-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2004 UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "04256-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2003 UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "04099-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2002 UFDS/N-SSATS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "03819-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="1998 UFDS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "03050-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="1997 UFDS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "02995-0001-Codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


echo "<h3>N-MHSS/URS</h3>"
echo "National Mental Health"

TOTAL_SYB=52
FIRST_STR="Services Survey (N-MHSS)"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"
TOTAL_SYB=50
FIRST_STR="Current state response rates"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "nmhss_resp_rates.htm" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


echo ""
TOTAL_SYB=52
FIRST_STR="N-MHSS Questionnaires"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
YEAR=2010
while [ ${YEAR} -le 2018 ];do

if [  ${YEAR} -eq 2011 -o  ${YEAR} -eq 2013 ]

then
:

else


FIRST_STR="${YEAR} Questionnaire"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_nmhss_q.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"
fi
let YEAR=YEAR+1
done

echo ""| tee -a

TOTAL_SYB=52
FIRST_STR="Data/Reports"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"
TOTAL_SYB=50
YEAR=2010
while [ ${YEAR} -le 2017 ];do

if [  ${YEAR} -eq 2011 -o  ${YEAR} -eq 2013 ]

then
:

else


FIRST_STR="${YEAR} N-MHSS Report"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_nmhss_rpt.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

fi 
let YEAR=YEAR+1
done

echo "Public Use Files"
echo ""
TOTAL_SYB=52
FIRST_STR="N-MHSS Codebook"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
YEAR=2010
while [ ${YEAR} -le 2014 ];do

if [  ${YEAR} -eq 2011 -o  ${YEAR} -eq 2013 ]

then
:

else


FIRST_STR="${YEAR} Codebook"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_nmhss_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

fi

let YEAR=YEAR+1
done


FIRST_STR="2015 Codebook"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2015_n_mhss_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2016 Codebook"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2016_nmhss_puf_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"


FIRST_STR="2017 Codebook"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2017_nmhss_puf_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""

TOTAL_SYB=52
FIRST_STR="N-MHSS State Profiles"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="2017 N-MHSS State Profiles"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2017_nmhss_st_profiles.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""

TOTAL_SYB=52
FIRST_STR="N-MHSS Individual State Profiles"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
FIRST_STR="State Profiles"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "tabYearDotChooseYearNmhssStateProfile" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""| tee -a


echo ""| tee -a
 echo "Uniform Reporting System (URS)"
echo ""
TOTAL_SYB=52
FIRST_STR="Output Tables"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
YEAR=2017
while [ 2007 -le ${YEAR} ];do

FIRST_STR="${YEAR} URS"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "${YEAR}_urs_output_tables_508.zip" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR-1
done


echo ""
echo "SMI/SED Prevalence Estimates"
echo ""
TOTAL_SYB=52
FIRST_STR="Output Tables"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
YEAR=2017
while [ 2013 -le ${YEAR} ];do

FIRST_STR="${YEAR} SMI/SED Prevalence Estimates"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "adult_smi_child_sed_prev_${YEAR}.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

let YEAR=YEAR-1
done

echo "<h3>MH-CLD</h3>"
echo "MH-CLD Manual/Crosswalk Template"
echo ""
TOTAL_SYB=52
FIRST_STR="Output Tables"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"


FIRST_STR="MH-CLD Instruction Manual Version 2.6"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "MH-CLD-Final-InstructionManual-Version2-6.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="MH-CLD Data Crosswalk Template"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "State-MH-CLD-DataCrosswalkTemplate.xlsx" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})               
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"
echo ""
echo "MH-CLD Data/Reports"
TOTAL_SYB=52
echo ""
FIRST_STR="Output Tables"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

FIRST_STR="2016 MH-CLD Report"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2016_Mental_Health_Annual_Report.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2015 MH-CLD Report"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2015_Mental_Health_Client_Level_Data_Report.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

FIRST_STR="2014 MH-CLD Report"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "2014_mental_health_client_level_data_report.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"

echo ""
echo "Public Use Files - MH-CLD"
echo ""
TOTAL_SYB=52
FIRST_STR="MH-CLD Codebook"
END_STR=Hits
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#END_STR})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "<pre id="p01">$FIRST_STR `python -c "print(' ' * $NEW_TOTAL_SYB )"` $END_STR</Pre>"

TOTAL_SYB=50
YEAR="2013-2016"
FIRST_STR="${YEAR} Codebook"
TOTAL_HITS="$(echo  "$(/usr/bin/grep -i "mhcld_13_16_codebook.pdf" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l)")"
LEN_FIRST_STR=$(echo ${#FIRST_STR})
LEN_END_STR=$(echo ${#TOTAL_HITS})
ADD_LEN_STR=$(expr $LEN_FIRST_STR + $LEN_END_STR)
NEW_TOTAL_SYB=$(expr $TOTAL_SYB - $ADD_LEN_STR)
echo "$FIRST_STR `python -c "print('-' * $NEW_TOTAL_SYB )"` $TOTAL_HITS"



#echo ""| tee -a
# echo "URS OUTPUT TABLES - INDIVIDUAL STATES, DC, AND TERRITORIES (PDF)"| tee -a

#echo  "$(/usr/bin/grep -i "" ${ACCESS_LOG_FILE} | awk '{print $8 "\t"  $9 }' | grep -i "\b200\b" | wc -l) ${HITS_ON}  Alabama"| tee -a
echo "</pre>"

echo "</body>
</html>"
