#!/bin/bash
##Purpose of this script is to combine the hit count script and  run them.

DIR=/local/apps/bhsis/scripts


TODAY_DATE=$(date +%d)
YESTERDAY_DATE=$(date --date='yesterday' +%d)


if [[ ${YESTERDAY_DATE} == '28' ]] || [[ ${YESTERDAY_DATE} == '29' ]] || [[ ${YESTERDAY_DATE} == '30' ]] || [[ ${YESTERDAY_DATE} == '31' ]]

then
	if [[ ${TODAY_DATE} == '01' ]]
	then
		echo "Month's end date is ${YESTERDAY_DATE}"	

		python ${DIR}/get_nginx_week_log.py --days 7

		python  ${DIR}/run_hits_on_script.py --days ${YESTERDAY_DATE}

		python ${DIR}/sends_attachment.py
	else
		echo "Today's date is not 1"
	
	fi

else

echo "Month has not been end yet"


fi


