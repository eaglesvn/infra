#!/bin/bash
##Created by Pavan Kumar
##This script download the files from locator application to /data-extract/locator location


HOST_URL="https://bhprd1.eagletechva.com"
ADDR_QUERY="locatorExcel?sAddr=39.0839994%2C%20-77.15278130000002&sType=both&includeServices=Y"


KEY_FILE="/local/apps/bhsis/scripts/download_locator_logs/download.key"

CURL_OUTPUT_FILE=/local/apps/bhsis/scripts/download_locator_logs/curl_url_output.txt
DOWNLOAD_LOG_FILE=/local/apps/bhsis/scripts/download_locator_logs/curl_url_output.log


rm -f ${KEY_FILE}

if [[ -f "${CURL_OUTPUT_FILE}" ]]
then
rm -f ${CURL_OUTPUT_FILE}

else

:

fi

/usr/bin/curl -k  --show-error --stderr ${CURL_OUTPUT_FILE} -v -o ${KEY_FILE} ${HOST_URL}/${ADDR_QUERY}

KEY_WITH_URL=$(grep "excel" ${KEY_FILE} |awk -F: '{print $2}'| awk -F} '{print $1}' |awk -F\" '{print $2}' | awk -F \\ '{print $2 $3}')


echo "key with url $KEY_WITH_URL"
cd  /data-extract/locator
/usr/bin/curl -k  --show-error -v -O --remote-header-name  --stderr ${CURL_OUTPUT_FILE} ${HOST_URL}${KEY_WITH_URL}

sed  "s///g" ${CURL_OUTPUT_FILE} > ${DOWNLOAD_LOG_FILE}

mailx -s "Locator file get download to the data-extract" pavan.kumar@eagletechva.com < ${DOWNLOAD_LOG_FILE}
