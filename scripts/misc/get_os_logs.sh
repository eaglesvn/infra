#!/bin/bash
#Created by Pavan Kumar
##Purpose of this script is to send out the Linux operating system log files to archive server. 
##Change the DB Hosts and host names varibles, please make sure you ssh key is working. 

ID=pavan.kumar
ARCHIVE_BACKUP_DIR="/bk2/log_files"

HOST_NAMES="$(cat /home/pavan.kumar/scripts/all_prod_hosts)"
EMAIL_ADDRS="pavan.kumar@eagletechva.com, bhsis_infra@eagletechva.com"
EMAIL_ADDRS2="pavan.kumar@eagletechva.com, Mubarak.Alabi@eagletechva.com"
DATE="$(date +%F_%T)"
DB_HOST_NAMES="$(cat /home/pavan.kumar/scripts/prod_db_hosts)"

function get_audit_log_files(){
  LOG_FILE_DIR="/var/log/audit"
  LOG_FILE_PATTERN="audit.log.*"
  CURRENT_LOG_FILE="audit.log"
  FIND_OLD_DAYS=7
  LOG_NAME=audit
  DATE=$(date +%F_%T)
  LOG="/tmp/get_os_logs_${LOG_NAME}_${DATE}.txt"
  EMAIL_LOG="/tmp/email_get_os_logs_${LOG_NAME}_${DATE}.txt"

  for host_name in ${HOST_NAMES}
  do
  echo ""  | tee -a ${LOG}
  echo "**********----------**********----------**********" | tee -a ${LOG}
  echo ""  | tee -a ${LOG}
  echo "Getting the ${LOG_NAME} log file from Host: ${host_name}" | tee -a ${LOG}
  echo "" | tee -a ${LOG}

    if [ ! -d "${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" ]
    then
      echo "Creating the Directory: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
      mkdir -p ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
      if [ "$?" -ne "0" ]
      then
        sudo mkdir -p ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
        echo "Creating the Directory: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
        sudo chown -R ${ID}:${ID} ${ARCHIVE_BACKUP_DIR}
        echo "Changing the ownership to ${ID}: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
      fi
    fi

    ssh -q ${ID}@${host_name} sudo chmod 755 ${LOG_FILE_DIR}
    echo "Changing the permission of dir: sudo chmod 755 ${LOG_FILE_DIR}"  | tee -a ${LOG}
    ssh -q ${ID}@${host_name} sudo chmod 644 ${LOG_FILE_DIR}/${CURRENT_LOG_FILE} 
    scp -qp ${ID}@${host_name}:${LOG_FILE_DIR}/${CURRENT_LOG_FILE} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
    if [ "$?" -ne "0" ]
    then
      echo "Failed: Not able to get the ${LOG_FILE_DIR}/${CURRENT_LOG_FILE} from ${host_name} to archive server." >>  ${EMAIL_LOG}
      echo "Failed: Not able to get the ${LOG_FILE_DIR}/${CURRENT_LOG_FILE} from ${host_name} to archive server." | tee -a ${LOG}
    fi
    echo "Getting the :${LOG_FILE_DIR}/${CURRENT_LOG_FILE} at location  ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
    ssh -q ${ID}@${host_name} sudo chmod 600 ${LOG_FILE_DIR}/${CURRENT_LOG_FILE}

    ssh -q  ${ID}@${host_name} sudo find ${LOG_FILE_DIR} -name ${LOG_FILE_PATTERN} -mtime -1 -type f | tee -a /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt
    #echo "Last seven days log files are:" | tee -a ${LOG}
    cat  /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt | tee -a ${LOG}

    if [ -s "/tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt" ]
    then

      for log_name in $(cat /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt)
      do
        echo "Log File name: $log_name" | tee -a ${LOG}
        ssh -q ${ID}@${host_name} sudo chmod 644 ${log_name}
        echo "Changing the permission of file: sudo chmod 644 ${log_name}"  | tee -a ${LOG}
        scp -qp ${ID}@${host_name}:${log_name} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
        if [ "$?" -ne "0" ]
        then
          echo "Failed: Not able to get the ${LOG_NAME} log file ${log_name} from ${host_name} to archive server." >>  ${EMAIL_LOG}
          echo "Failed: Not able to get the ${LOG_NAME} log file ${log_name} from ${host_name} to archive server." | tee -a ${LOG}
        fi       
        echo "SCP: ${ID}@${host_name}:${log_name} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/" | tee -a ${LOG}
        ssh -q ${ID}@${host_name} sudo chmod 400 ${log_name}
        echo "Changing the permission of file back to 400: sudo chmod 400 ${log_name}"  | tee -a ${LOG}
        log_file_base=$(basename ${log_name})
        log_file_date=$(date -d "@$(stat -c '%Y' ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/${log_file_base})" '+%Y_%m_%d')
        echo "Audit Log File Date: ${log_file_date}"
        gzip -S "_${log_file_date}.gz" -f ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/${log_file_base}
        echo "Compressed file: $(ls ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/${log_file_base}_${log_file_date}.gz)" | tee -a ${LOG}
      done

      ssh -q ${ID}@${host_name} sudo chmod 700 ${LOG_FILE_DIR}
      echo "Changing the permission of dir back to 700 : sudo chmod 700 ${LOG_FILE_DIR}"  | tee -a ${LOG}
    fi
  done

  if [ -s ${EMAIL_LOG} ]
  then
    mailx -s "Failed: Getting the ${LOG_NAME} to archive server is failed, please look at it" ${EMAIL_ADDRS} < ${EMAIL_LOG} 
  fi
  if [ -s ${LOG} ]
  then
    mailx -s "${LOG_NAME} log files to archive server: " ${EMAIL_ADDRS2} < ${LOG}
  fi 
  
}

function get_messages_log_files(){
  LOG_FILE_DIR="/var/log"
  LOG_FILE_PATTERN="messages-*"
  FIND_OLD_DAYS=7
  LOG_NAME=messages
  CURRENT_LOG_FILE="messages"
  DATE=$(date +%F_%T)
  LOG="/tmp/get_os_logs_${LOG_NAME}_${DATE}.txt"
  EMAIL_LOG="/tmp/email_get_os_logs_${LOG_NAME}_${DATE}.txt"
  for host_name in ${HOST_NAMES}
  do
  echo ""  | tee -a ${LOG}
  echo "**********----------**********----------**********" | tee -a ${LOG}
  echo ""  | tee -a ${LOG}
  echo "Getting the ${LOG_NAME} log file from Host: ${host_name}" | tee -a ${LOG}
  echo "" | tee -a ${LOG}

    if [ ! -d "${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" ]
    then
      echo "Creating the Directory: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
      mkdir -p ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
      if [ "$?" -ne "0" ]
      then
        sudo mkdir -p ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
        echo "Creating the Directory: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
         sudo chown -R ${ID}:${ID} ${ARCHIVE_BACKUP_DIR}
        echo "Changing the ownership to ${ID}: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
      fi
    fi

    ssh -q ${ID}@${host_name} sudo chmod 644 ${LOG_FILE_DIR}/${CURRENT_LOG_FILE}
    scp -qp ${ID}@${host_name}:${LOG_FILE_DIR}/${CURRENT_LOG_FILE} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
    if [ "$?" -ne "0" ]
    then
      echo "Failed: Not able to get the ${LOG_FILE_DIR}/${CURRENT_LOG_FILE} from ${host_name} to archive server." >>  ${EMAIL_LOG}
      echo "Failed: Not able to get the ${LOG_FILE_DIR}/${CURRENT_LOG_FILE} from ${host_name} to archive server." | tee -a ${LOG}
    fi
    echo "Getting the :${LOG_FILE_DIR}/${CURRENT_LOG_FILE} at location  ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
    ssh -q ${ID}@${host_name} sudo chmod 644 ${LOG_FILE_DIR}/${CURRENT_LOG_FILE}

  ssh -q  ${ID}@${host_name} sudo find ${LOG_FILE_DIR} -name ${LOG_FILE_PATTERN} -mtime -1 -type f | tee -a /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt
    #echo "Last seven days log files are:" | tee -a ${LOG}
    cat  /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt | tee -a ${LOG}

    if [ -s "/tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt" ]
    then
      for log_name in $(cat /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt)
      do
        echo "Log File name: $log_name" | tee -a ${LOG}
        ssh -q ${ID}@${host_name} sudo chmod 644 ${log_name}
        echo "Changing the permission of file: sudo chmod 644 ${log_name}"  | tee -a ${LOG}
        scp -qp ${ID}@${host_name}:${log_name} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
        if [ "$?" -ne "0" ]
        then
          echo "Failed: Not able to get the ${LOG_NAME} log file ${log_name} from ${host_name} to archive server." >>  ${EMAIL_LOG}
          echo "Failed: Not able to get the ${LOG_NAME} log file ${log_name} from ${host_name} to archive server." | tee -a ${LOG}
        fi
        echo "SCP: ${ID}@${host_name}:${log_name} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/" | tee -a ${LOG}
        ssh -q ${ID}@${host_name} sudo chmod 400 ${log_name}
        echo "Changing the permission of file back to 400: sudo chmod 400 ${log_name}"  | tee -a ${LOG}
        log_file_base=$(basename ${log_name})
        gzip -f ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/${log_file_base}
        echo "Compressed file: $(ls ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/${log_file_base}.gz)" | tee -a ${LOG}

      done
    fi
  done

  if [ -s ${EMAIL_LOG} ]
  then
    mailx -s "Failed: Getting the ${LOG_NAME} to archive server is failed, please look at it" ${EMAIL_ADDRS} < ${EMAIL_LOG}
  fi
  if [ -s ${LOG} ]
  then
    mailx -s "${LOG_NAME} log files to archive server: " ${EMAIL_ADDRS2} < ${LOG}
  fi
}

function get_secure_log_files(){
  LOG_FILE_DIR="/var/log"
  LOG_FILE_PATTERN="secure-*"
  FIND_OLD_DAYS=7
  LOG_NAME=secure
  CURRENT_LOG_FILE="secure"
  DATE=$(date +%F_%T)
  LOG="/tmp/get_os_logs_${LOG_NAME}_${DATE}.txt"
  EMAIL_LOG="/tmp/email_get_os_logs_${LOG_NAME}_${DATE}.txt"
  for host_name in ${HOST_NAMES}
  do
  echo ""  | tee -a ${LOG}
  echo "**********----------**********----------**********" | tee -a ${LOG}
  echo ""  | tee -a ${LOG}
  echo "Getting the ${LOG_NAME} log file from Host: ${host_name}" | tee -a ${LOG}
  echo "" | tee -a ${LOG}

    if [ ! -d "${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" ]
    then
      echo "Creating the Directory: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
      mkdir -p ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
      if [ "$?" -ne "0" ]
      then
        sudo mkdir -p ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
        echo "Creating the Directory: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
         sudo chown -R ${ID}:${ID} ${ARCHIVE_BACKUP_DIR}
        echo "Changing the ownership to ${ID}: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
      fi
    fi

    ssh -q ${ID}@${host_name} sudo chmod 644 ${LOG_FILE_DIR}/${CURRENT_LOG_FILE}
    scp -qp ${ID}@${host_name}:${LOG_FILE_DIR}/${CURRENT_LOG_FILE} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
    if [ "$?" -ne "0" ]
    then
      echo "Failed: Not able to get the ${LOG_FILE_DIR}/${CURRENT_LOG_FILE} from ${host_name} to archive server." >>  ${EMAIL_LOG}
      echo "Failed: Not able to get the ${LOG_FILE_DIR}/${CURRENT_LOG_FILE} from ${host_name} to archive server." | tee -a ${LOG}
    fi
    echo "Getting the :${LOG_FILE_DIR}/${CURRENT_LOG_FILE} at location  ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
    ssh -q ${ID}@${host_name} sudo chmod 644 ${LOG_FILE_DIR}/${CURRENT_LOG_FILE}

    ssh -q  ${ID}@${host_name} sudo find ${LOG_FILE_DIR} -name ${LOG_FILE_PATTERN} -mtime -1 -type f | tee -a /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt
    #echo "Last seven days log files are:" | tee -a ${LOG}
    cat  /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt | tee -a ${LOG}

    if [ -s "/tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt" ]
    then
      for log_name in $(cat /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt)
      do
        echo "Log File name: $log_name" | tee -a ${LOG}
        ssh -q ${ID}@${host_name} sudo chmod 644 ${log_name}
        echo "Changing the permission of file: sudo chmod 644 ${log_name}"  | tee -a ${LOG}
        scp -qp ${ID}@${host_name}:${log_name} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
        if [ "$?" -ne "0" ]
        then
          echo "Failed: Not able to get the ${LOG_NAME} log file ${log_name} from ${host_name} to archive server." >>  ${EMAIL_LOG}
          echo "Failed: Not able to get the ${LOG_NAME} log file ${log_name} from ${host_name} to archive server." | tee -a ${LOG}
        fi
        echo "SCP: ${ID}@${host_name}:${log_name} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/" | tee -a ${LOG}
        ssh -q ${ID}@${host_name} sudo chmod 400 ${log_name}
        echo "Changing the permission of file back to 400: sudo chmod 400 ${log_name}"  | tee -a ${LOG}
        log_file_base=$(basename ${log_name})
        gzip -f  ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/${log_file_base}
        echo "Compressed file: $(ls ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/${log_file_base}.gz)" | tee -a ${LOG}

      done
    fi
  done

  if [ -s ${EMAIL_LOG} ]
  then
    mailx -s "Failed: Getting the ${LOG_NAME} to archive server is failed, please look at it" ${EMAIL_ADDRS} < ${EMAIL_LOG}
  fi
  if [ -s ${LOG} ]
  then
    mailx -s "${LOG_NAME} log files to archive server: " ${EMAIL_ADDRS2} < ${LOG}
  fi

}

function get_postgres_log_files(){
  LOG_FILE_DIR="/var/log"
  LOG_FILE_PATTERN="postgresql.log-*"
  FIND_OLD_DAYS=7
  LOG_NAME=postgres
  CURRENT_LOG_FILE="postgresql.log"
  DATE=$(date +%F_%T)
  LOG="/tmp/get_os_logs_${LOG_NAME}_${DATE}.txt"
  EMAIL_LOG="/tmp/email_get_os_logs_${LOG_NAME}_${DATE}.txt"

  for host_name in ${DB_HOST_NAMES}
  do
  echo ""  | tee -a ${LOG}
  echo "**********----------**********----------**********" | tee -a ${LOG}
  echo ""  | tee -a ${LOG}
  echo "Getting the ${LOG_NAME} log file from Host: ${host_name}" | tee -a ${LOG}
  echo "" | tee -a ${LOG}

    if [ ! -d "${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" ]
    then
      echo "Creating the Directory: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
      mkdir -p ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
      if [ "$?" -ne "0" ]
      then
        sudo mkdir -p ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
        echo "Creating the Directory: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
         sudo chown -R ${ID}:${ID} ${ARCHIVE_BACKUP_DIR}
        echo "Changing the ownership to ${ID}: ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
      fi
    fi

    ssh -q ${ID}@${host_name} sudo chmod 644 ${LOG_FILE_DIR}/${CURRENT_LOG_FILE}
    scp -qp ${ID}@${host_name}:${LOG_FILE_DIR}/${CURRENT_LOG_FILE} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
    if [ "$?" -ne "0" ]
    then
      echo "Failed: Not able to get the ${LOG_FILE_DIR}/${CURRENT_LOG_FILE} from ${host_name} to archive server." >>  ${EMAIL_LOG}
      echo "Failed: Not able to get the ${LOG_FILE_DIR}/${CURRENT_LOG_FILE} from ${host_name} to archive server." | tee -a ${LOG}
    fi
    echo "Getting the :${LOG_FILE_DIR}/${CURRENT_LOG_FILE} at location  ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}" | tee -a ${LOG}
    ssh -q ${ID}@${host_name} sudo chmod 644 ${LOG_FILE_DIR}/${CURRENT_LOG_FILE}
    ssh -q  ${ID}@${host_name} sudo find ${LOG_FILE_DIR} -name ${LOG_FILE_PATTERN} -mtime -1 -type f | tee -a /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt
    #echo "Last seven days log files are:" | tee -a ${LOG}
    cat  /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt | tee -a ${LOG}

    if [ -s "/tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt" ]
    then
      for log_name in $(cat /tmp/${host_name}_${LOG_NAME}_logs_${DATE}.txt)
      do
        echo "Log File name: $log_name" | tee -a ${LOG}
        ssh -q ${ID}@${host_name} sudo chmod 644 ${log_name}
        echo "Changing the permission of file: sudo chmod 644 ${log_name}"  | tee -a ${LOG}
        scp -pq ${ID}@${host_name}:${log_name} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}
        if [ "$?" -ne "0" ]
        then
          echo "Failed: Not able to get the ${LOG_NAME} log file ${log_name} from ${host_name} to archive server." >>  ${EMAIL_LOG}
          echo "Failed: Not able to get the ${LOG_NAME} log file ${log_name} from ${host_name} to archive server." | tee -a ${LOG}
        fi
        echo "SCP: ${ID}@${host_name}:${log_name} ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/" | tee -a ${LOG}
        ssh -q ${ID}@${host_name} sudo chmod 400 ${log_name}
        echo "Changing the permission of file back to 400: sudo chmod 400 ${log_name}"  | tee -a ${LOG}
        log_file_base=$(basename ${log_name})
        gzip -f ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/${log_file_base}
        echo "Compressed file: $(ls ${ARCHIVE_BACKUP_DIR}/${LOG_NAME}/${host_name}/${log_file_base}.gz)" | tee -a ${LOG}

      done
    fi
  done

  if [ -s ${EMAIL_LOG} ]
  then
    mailx -s "Failed: Getting the ${LOG_NAME} to archive server is failed, please look at it" ${EMAIL_ADDRS} < ${EMAIL_LOG}
  fi
  if [ -s ${LOG} ]
  then
    mailx -s "${LOG_NAME} log files to archive server: " ${EMAIL_ADDRS2} < ${LOG}
  fi

}


get_audit_log_files
get_messages_log_files
get_secure_log_files
get_postgres_log_files


