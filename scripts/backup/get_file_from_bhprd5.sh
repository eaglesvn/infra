#!/bin/bash
##Created By Pavan Kumar
##This script copies the nginx access log files of locator to backup server 

ID="bkuser"
HOSTNAME="bhprd5dp01.eagletechva.com"
REMOTE_PATH="/local/apps/bhsis"
DATE=$(date +%F-%T)
LOG_FILE=/tmp/bhprd5dp01_rsync_${DATE}.log
LOCAL_PATH="/bk1/bhprd5dp01_files"
EMAIL_ID=pavan.kumar@eagletechva.com

function get_files {

rsync --ignore-existing  -r -a -v -e ssh ${ID}@${HOSTNAME}:${REMOTE_PATH} ${LOCAL_PATH} --log-file=${LOG_FILE}



}

function checking_the_error_on_log_file {

FOUND_ERROR=$(grep -Ei "error| failed | fail" ${LOG_FILE})

if [[ ${FOUND_ERROR} != "" ]]
then

echo "bhprd5dp01  rsync is Failed: Please check" | mailx -s "FAILED: bhprd5dp01  rsync to backup server is Failed" pavan.kumar@eagletechva.com 


else 

echo "bhprd5dp01  rsync is completed" | mailx -s "SUCCESS: bhprd5dp01  rsync to backup server is completed" pavan.kumar@eagletechva.com
fi

}

get_files

checking_the_error_on_log_file
