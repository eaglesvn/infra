#!/bin/sh
## Purpose of this script is to rsync bk1 bk2 bk3 directories on (bhres1arch01) to bk1 bk2 bk3 directories on (bhwdc1arch010)
## RSYNC has to be install on Both Servers

#Variables
#####################################################################################
BACKUP_SERVER_ID=bkuser

MAIN_SERVER_PATH1=/bk1/
MAIN_SERVER_PATH2=/bk2/
MAIN_SERVER_PATH3=/bk3/
MAIN_SERVER_PATH4=/bk4/


BACKUP_SERVER_HOSTNAME=bhwdc1arch01.eagletechva.com

BACKUP_SERVER_PATH1=/bk1/
BACKUP_SERVER_PATH2=/bk2/
BACKUP_SERVER_PATH3=/bk3/
BACKUP_SERVER_PATH4=/bk4/


DATE=$(date +%Y-%m-%d_%H-%M-%S)
#EMAIL_ADDR="mubarak.alabi@eagletechva.com"
EMAIL_ADDR="mubarak.alabi@eagletechva.com, bhsis_infra@eagletechva.com"
#HOST_NAME=$(hostname | awk -F. '{print $1}')
HOST_NAME=bhres1arch01.eagletechva.com
LOG_FILE=/tmp/bk1234_rsync_backup_from_reston_archive_${HOST_NAME}_to_wdc_archive_${BACKUP_SERVER_HOSTNAME}_${DATE}.txt
####################################################################################


if [[ -f ${LOG_FILE} ]]
then

rm -f ${LOG_FILE}

else

:

fi

rsync --delete-before -avzhde ssh ${MAIN_SERVER_PATH1} ${BACKUP_SERVER_ID}@${BACKUP_SERVER_HOSTNAME}:${BACKUP_SERVER_PATH1}  --log-file=${LOG_FILE}
rsync -avzhde ssh ${MAIN_SERVER_PATH2} ${BACKUP_SERVER_ID}@${BACKUP_SERVER_HOSTNAME}:${BACKUP_SERVER_PATH2}  --log-file=${LOG_FILE}
rsync --delete-before -avzhde ssh ${MAIN_SERVER_PATH3} ${BACKUP_SERVER_ID}@${BACKUP_SERVER_HOSTNAME}:${BACKUP_SERVER_PATH3}  --log-file=${LOG_FILE}
rsync --delete-before -avzhde ssh ${MAIN_SERVER_PATH4} ${BACKUP_SERVER_ID}@${BACKUP_SERVER_HOSTNAME}:${BACKUP_SERVER_PATH4}  --log-file=${LOG_FILE}



grep "failed" ${LOG_FILE}

RSYNC_FAILED=$?

grep "error" ${LOG_FILE}

RSYNC_ERROR=$?


if [[ "${RSYNC_FAILED}" -eq "0" ]] && [[ "${RSYNC_ERROR}" -eq "0" ]]
then

STATUS=FAILED

else

STATUS=SUCCESSFUL

fi


MESSAGE="${STATUS} /bk1234 Rsync Backup from ${HOST_NAME} to ${BACKUP_SERVER_HOSTNAME} /bk1234"

echo "$MESSAGE" | mail -s "${MESSAGE}" ${EMAIL_ADDR} < ${LOG_FILE}

killall -SIGTERM rsync
exit 0 
