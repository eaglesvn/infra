#!/bin/sh
### Created by Pavan Kumar
### Purpose of this script is to choose Release version and Platform where need to be deployed

. ${play_main_dir}/projects/${project_name}/${project_name}_env.properties


### Calling Environment menu for deployment


function deploy_menu () {

tput clear 
tput setf 5
printf "* * * * * * * ${project_name} Deploy * * * * * * *
	--------------------------------------------------
	[1] BHPRD1 
	[0] EXIT From this Menu, Called Main Menu Again
	----------------------------------------------\n"
tput sgr0
tput setf 2
  printf "Enter your menu choice [1-0]:"

tput sgr0
  read yourch
  case $yourch in

   1)

tput setf 4
printf "You have select PROD environment for deployment\n"
sleep 2
printf "\nAre you sure you want to PROD deployment , if yes type YES in capital to continue: "
tput sgr0
read prdans

if [[ ${prdans} == "YES" ]];then

printf "You have selected production environment\n"
sleep 2
:

else 
tput setf 4
printf "You will be out of deployment menu, because by mistake you selected production environment\n"
tput sgr0
exit 1

fi 


printf "\nEnter your unix ID: "
read USER_ID
export USER_ID;

printf "\nEnter your unix ID password: "

read  -rs ID_PASS

export ID_PASS;

        tput clear
        tput setf 5
        printf "* * * * * * * ${project_name} Deploy Menu for PROD Environment * * * * * * *
        --------------------------------------------------
        [1] Prod Deployment
        [2] Check Status
        [3] Stop the Apps
        [4] Start the Apps
        [5] ReStart Apps

        [0] EXIT
        ----------------------------------------------\n"
        tput sgr0
        tput setf 2
        printf "Enter your menu choice [1-2]:"

        tput sgr0
        read prd_chs
        case ${prd_chs} in

                1)

                ENV_NAME=bhprd1
		

		printf "DO you want to deploy this release, to 'SETA' servers or 'SETB 'servers or to individual servers,\n
then  pass the below argument, and do not use the FQDN when typeing the servers name.\n\n"
printf "Type SETA, SETB or servers name with space separated: "
		read DEPLOY_TO
		
		if [[ ${DEPLOY_TO} == "ALL" ]];then
		echo "YOu have enterd the ALL, which is incorrect"
		exit 1
		

		. ${play_main_dir}/deploy/get_pkgs.sh
                PROP_FILE_PATH=${play_main_dir}/projects/${project_name}/${ENV_NAME}/*.properties

		echo "Release Tag Dir in conf $REL_TAG_DIR"
		export REL_TAG_DIR
                for dp_prop_file in $( /usr/bin/ls $PROP_FILE_PATH )

                do

	                . ${play_dp_scripts_dir}/play_deploy.sh ${dp_prop_file}  ${REL_TAG_DIR} 

                done
		

		elif [[ ${DEPLOY_TO} == "SETA" ]];then
			
		
		
		 . ${play_main_dir}/deploy/get_pkgs.sh
		
		for host in ${SETA_HOSTNAMES} 

		do
			
			echo "Host name is $host"
			dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties

	                echo "Release Tag Dir in conf $REL_TAG_DIR"
	                export REL_TAG_DIR

			 . ${play_dp_scripts_dir}/play_deploy.sh ${dp_prop_file}  ${REL_TAG_DIR} 	


		done
		

                elif [[ ${DEPLOY_TO} == "SETB" ]];then


                 . ${play_main_dir}/deploy/get_pkgs.sh

                for host in ${SETB_HOSTNAMES}

                do

                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties

                        echo "Release Tag Dir in conf $REL_TAG_DIR"
                        export REL_TAG_DIR

                         . ${play_dp_scripts_dir}/play_deploy.sh ${dp_prop_file}  ${REL_TAG_DIR} 


                done


		 
		else

		
			
                . ${play_main_dir}/deploy/get_pkgs.sh
                for host in ${DEPLOY_TO}

                do
			if [[ -f ${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties ]]
			then

                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties

                        echo "Release Tag Dir in conf $REL_TAG_DIR"
                        export REL_TAG_DIR

                         . ${play_dp_scripts_dir}/play_deploy.sh ${dp_prop_file}  ${REL_TAG_DIR} 

			else
				printf "Property file for Host $host does not exist\n"

				exit 1
			fi


                done



		fi

                ;;

                2)

 
                ENV_NAME=bhprd1
                printf "Type, 'ALL', 'SETA' or 'SETB ' to see the status off aplications,\n
For only particular application servers use the server name with space do not use the FQDN name.\n\n"
printf "Type ALL, SETA, SETB or servers name with space seprated without domain name: "
                read DEPLOY_TO
                if [[ ${DEPLOY_TO} == "ALL" ]];then
                PROP_FILE_PATH=${play_main_dir}/projects/${project_name}/${ENV_NAME}/*.properties
                for dp_prop_file in $( /usr/bin/ls $PROP_FILE_PATH )
                do
                        . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} STATUS
                done
                elif [[ ${DEPLOY_TO} == "SETA" ]];then
                for host in ${SETA_HOSTNAMES}
                do
                        echo "Host name is $host"
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} STATUS
                done
                elif [[ ${DEPLOY_TO} == "SETB" ]];then
                for host in ${SETB_HOSTNAMES}
                do
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} STATUS
                done
                else
                for host in ${DEPLOY_TO}
                do
                        if [[ -f ${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties ]]
                        then
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} STATUS
                        else
                                printf "Property file for Host $host does not exist\n"
                                exit 1
                        fi
                done
                fi

                   . ${play_dp_scripts_dir}/deploy_conf_setup.sh
                ;;

                3)


                ENV_NAME=bhprd1
                printf "Typeo'ALL', 'SETA' or 'SETB ' to see the status off aplications,\n
For only particular application servers use the server name with space do not use the FQDN name.\n\n"
printf "Type ALL, SETA, SETB or servers name with space seprated without domain name: "
                read DEPLOY_TO
                if [[ ${DEPLOY_TO} == "ALL" ]];then
                PROP_FILE_PATH=${play_main_dir}/projects/${project_name}/${ENV_NAME}/*.properties
                for dp_prop_file in $( /usr/bin/ls $PROP_FILE_PATH )
                do
                        . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} STOP
                done
                elif [[ ${DEPLOY_TO} == "SETA" ]];then
                for host in ${SETA_HOSTNAMES}
                do
                        echo "Host name is $host"
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} STOP
                done
                elif [[ ${DEPLOY_TO} == "SETB" ]];then
                for host in ${SETB_HOSTNAMES}
                do
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} STOP
                done
                else
                for host in ${DEPLOY_TO}
                do
                        if [[ -f ${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties ]]
                        then
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} STOP
                        else
                                printf "Property file for Host $host does not exist\n"
                                exit 1
                        fi
                done
                fi

                   . ${play_dp_scripts_dir}/deploy_conf_setup.sh

                ;;



                4)


                ENV_NAME=bhprd1
                printf "Typeo'ALL', 'SETA' or 'SETB ' to see the status off aplications,\n
For only particular application servers use the server name with space do not use the FQDN name.\n\n"
printf "Type ALL, SETA, SETB or servers name with space seprated without domain name: "
                read DEPLOY_TO
                if [[ ${DEPLOY_TO} == "ALL" ]];then
                PROP_FILE_PATH=${play_main_dir}/projects/${project_name}/${ENV_NAME}/*.properties
                for dp_prop_file in $( /usr/bin/ls $PROP_FILE_PATH )
                do
                        . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} START
                done
                elif [[ ${DEPLOY_TO} == "SETA" ]];then
                for host in ${SETA_HOSTNAMES}
                do
                        echo "Host name is $host"
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} START
                done
                elif [[ ${DEPLOY_TO} == "SETB" ]];then
                for host in ${SETB_HOSTNAMES}
                do
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} START
                done
                else
                for host in ${DEPLOY_TO}
                do
                        if [[ -f ${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties ]]
                        then
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} START
                        else
                                printf "Property file for Host $host does not exist\n"
                                exit 1
                        fi
                done
                fi

                   . ${play_dp_scripts_dir}/deploy_conf_setup.sh

                ;;

                5)


                ENV_NAME=bhprd1
                printf "Typeo'ALL', 'SETA' or 'SETB ' to see the status off aplications,\n
For only particular application servers use the server name with space do not use the FQDN name.\n\n"
printf "Type ALL, SETA, SETB or servers name with space seprated without domain name: "
                read DEPLOY_TO
                if [[ ${DEPLOY_TO} == "ALL" ]];then
                PROP_FILE_PATH=${play_main_dir}/projects/${project_name}/${ENV_NAME}/*.properties
                for dp_prop_file in $( /usr/bin/ls $PROP_FILE_PATH )
                do
                        . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} RESTART
                done
                elif [[ ${DEPLOY_TO} == "SETA" ]];then
                for host in ${SETA_HOSTNAMES}
                do
                        echo "Host name is $host"
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} RESTART
                done
                elif [[ ${DEPLOY_TO} == "SETB" ]];then
                for host in ${SETB_HOSTNAMES}
                do
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} RESTART
                done
                else
                for host in ${DEPLOY_TO}
                do
                        if [[ -f ${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties ]]
                        then
                        dp_prop_file=${play_main_dir}/projects/${project_name}/${ENV_NAME}/${host}.properties
                       . ${play_dp_scripts_dir}/play_app_status.sh ${dp_prop_file} RESTART
                        else
                                printf "Property file for Host $host does not exist\n"
                                exit 1
                        fi
                done
                fi

                   . ${play_dp_scripts_dir}/deploy_conf_setup.sh

                ;;

                0)
                exit 0
                ;;
                *)
                exit 0
                ;;

                esac
;;



   0) 
. /local/apps/bhsis/play2_app_deploy/main_menu.sh
       ;;
   *) 
      
   exit 0 
       ;;
   esac

}


deploy_menu
