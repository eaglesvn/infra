#!/bin/bash
## Created by Pavan Kumar
## Purpose of this script is to replace the variable in projects files, project files use few hard coded value, however these value can not be same for all the environment
## So this script replace @variable@ with environment required value.


usage()
{
cat << EOF
usage: $0 options

This script run with these parameters

OPTIONS:
        -p Property file path
        -d Directory path to replace variable
        -f File path to replace variable
        -h help message to use this script
EOF
}

PROPERTY_FILE=
REP_DIR=
REP_FILE=


if [[ $# -eq "" ]]

then

usage

exit 1

else
:

fi

while getopts "hp:d:f:" OPTION

do

case $OPTION in
        h) usage
                exit 1 ;;

        p) PROPERTY_FILE=$OPTARG
        ;;

        d) REP_DIR=$OPTARG
        ;;

        f) REP_FILE=$OPTARG
        ;;

        ?)
        usage
        exit
        ;;
esac

done

if [[ ${REP_DIR} != "" ]] && [[  ${REP_FILE} != "" ]]
then
tput setf 4
echo "Existing from Replace Varaiable script: you can not use Directory Path and File Path Same time"
tput sgr 0
exit 1

else
:

fi





for PROP_FILE in `cat ${PROPERTY_FILE}|grep -v "^#"`

do


TOKEN_NAME=`echo ${PROP_FILE} | awk -F= '{print $1}'`


TOKEN_VALUE=`echo ${PROP_FILE} | cut -d "=" -f2-`



if [[ ${REP_FILE} == "" ]] && [[  ${REP_DIR} != ""  ]]
then
find ${REP_DIR} -name "*.xml" -exec sed -i  's/@@'${TOKEN_NAME}'@@/'${TOKEN_VALUE}'/g' {} \;
find ${REP_DIR} -name "*.properties" -exec sed -i  's/@@'${TOKEN_NAME}'@@/'${TOKEN_VALUE}'/g' {} \;

find ${REP_DIR} -name "*.html" -exec sed -i  's/@@'${TOKEN_NAME}'@@/'${TOKEN_VALUE}'/g' {} \;

find ${REP_DIR} -name "*.conf" -exec sed -i  's/@@'${TOKEN_NAME}'@@/'${TOKEN_VALUE}'/g' {} \;

find ${REP_DIR} -name "*.gpf" -exec sed -i  's/@@'${TOKEN_NAME}'@@/'${TOKEN_VALUE}'/g' {} \;

elif [[  ${REP_FILE} != "" ]] && [[  ${REP_DIR} == ""  ]]
then

 sed -i  's/@@'${TOKEN_NAME}'@@/'${TOKEN_VALUE}'/g' ${REP_FILE}

else
 printf "Directory, and file path parameters are not passwd existing from menu/n"
exit 1

fi

done

