
echo "Performing comparisons between Database and Fusion Table"

# Get the number of facilities in the database
DATABASE_FACILITY_COUNT=$(psql -U locator -d etlocator -t -c "SELECT count(*) from locator.locator_facilities WHERE type_facility_code in (1,2,3)" | sed -e 's/[^0-9]//g')

# Get the json response for the query how many facilities in fusion table
FACILITY_COUNT_JSON_RESPONSE=$(curl "https://www.googleapis.com/fusiontables/v2/query?sql=SELECT+COUNT+()+FROM+${FUSION_TABLE_ID}&key=${GOOGLE_API_KEY}&jsonCallback=getData")

# Get the number of facilities in the Fusion Table from the json response
FUSION_TABLE_FACILITY_COUNT=$(echo "$FACILITY_COUNT_JSON_RESPONSE" | sed -e 's/[^0-9]//g' | sed -n '/[0-9]/p');

# Compare the DB value and Fusion Table value - exit if does not match
if [[ $DATABASE_FACILITY_COUNT -ne $FUSION_TABLE_FACILITY_COUNT ]]; then
    echo -e "Database facility count does not match Fusion Table facility count. Exiting.." > /dev/stderr;
    exit 1;
fi

echo "VALIDATION PASSED: locator_facilities table row count match"

# Get all of the states in the database
STATES=$(psql -U locator -d etlocator -t -c "SELECT DISTINCT state FROM locator.locator_facilities")
for state in $STATES
do
    ### QUERY 1 - Check number of facilities in each state matches ###
    # Get the number of facilities in the DB for this state
    DATABASE_STATE_FACILITY_COUNT=$(psql -U locator -d etlocator -t -c "SELECT count(*) FROM locator.locator_facilities WHERE state = '${state}' AND type_facility_code in (1,2,3)" | sed -e 's/[^0-9]//g')
    
    # Get the json response for the query how many facilities are in fusion table for this state
    STATE_FACILITY_COUNT_JSON_RESPONSE=$(curl "https://www.googleapis.com/fusiontables/v2/query?sql=SELECT+COUNT+()+FROM+${FUSION_TABLE_ID}+WHERE+%27state%27+=+%27${state}%27&key=${GOOGLE_API_KEY}&jsonCallback=getData")

    # Get the number of facilities in the Fusion Table for this state from the json response
    FUSION_TABLE_STATE_FACILITY_COUNT=$(echo "$STATE_FACILITY_COUNT_JSON_RESPONSE" | sed -e 's/[^0-9]//g' | sed -n '/[0-9]/p');
    
    # Compare the DB value and Fusion Table value - exit if does not match
    if [[ $DATABASE_STATE_FACILITY_COUNT -ne $FUSION_TABLE_STATE_FACILITY_COUNT ]]; then
        echo -e "Number of facilities in DB for state: ${state} does not match Fusion Table. Exiting.." > /dev/stderr;
        exit 1;
    fi

    echo "VALIDATION PASSED: Number of facilities in DB and FT match for state: ${state}"

    ### QUERY 2 - Check max frid for each state matches ###
    # Get the largest frid for this state
    DATABASE_STATE_MAX_FRID=$(psql -U locator -d etlocator -t -c "SELECT frid FROM locator.locator_facilities WHERE state = '${state}' AND type_facility_code IN (1,2) ORDER BY frid DESC LIMIT 1" | sed -e 's/ //g')

    # Get the json response for the query what is the largest frid in the fusion table for this state
    STATE_MAX_FRID_JSON_RESPONSE=$(curl "https://www.googleapis.com/fusiontables/v2/query?sql=SELECT+%27frid%27+FROM+${FUSION_TABLE_ID}+WHERE+%27state%27+=+%27${state}%27+AND+%27type_facility_code%27+IN+%281%2C2%29+ORDER+BY+%27frid%27+DESC+LIMIT+1&key=${GOOGLE_API_KEY}&jsonCallback=getData")

    # Get the max frid in the Fusion table for this state from the json response
    FUSION_TABLE_STATE_MAX_FRID=$(echo "$STATE_MAX_FRID_JSON_RESPONSE" | sed -e 's/[^A-Z0-9]//g' | sed -n '/[A-Z0-9]/p');

    # Compare the DB value and Fusion Table value - exit if does not match
    if [[ $FUSION_TABLE_STATE_MAX_FRID -ne $DATABASE_STATE_MAX_FRID ]]; then
        echo -e "Max frid in DB for state: ${state} does not match Fusion Table. Exiting.." > /dev/stderr;
        exit 1;
    fi

    echo "VALIDATION PASSED: Maximum frid in DB AND FT match for state: ${state}"
done

echo "VALIDATION PASSED"
