FROM centos:centos7.4.1708

ENV PGDATA /var/lib/pgsql/9.6/data/ 
ENV PATH /usr/pgsql-9.6/bin:$PATH
RUN export PATH
RUN locale
RUN localedef --force --inputfile=en_US --charmap=UTF-8 --alias-file=/usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.UTF-8
RUN locale


ENV SUMMARY="PostgreSQL is an advanced Object-Relational database management system" \
    DESCRIPTION="PostgreSQL image contains the client and server programs that you'll need to create, run, maintain and access a PostgreSQL DBMS server."

LABEL summary="$SUMMARY" \
  usage="docker run -d --name locatorpg -e POSTGRESQL_USER=locator -e POSTGRESQL_PASSWORD='ettst110wner' -e POSTGRESQL_DATABASE=etlocator -p 5432:5432 centospg:9.6" \
  maintainer="shailaja.arkacharya@eagletechva.com"
 
RUN yum -y update && \
    yum -y install sudo epel-release && \
    rpm -ivh http://yum.postgresql.org/9.6/redhat/rhel-7.4-x86_64/pgdg-centos96-9.6-3.noarch.rpm && \
    yum -y install postgresql96 postgresql96-contrib postgresql96-devel postgresql96-libs postgresql96-server -y --nogpgcheck && \
    yum -y install postgis24_96.x86_64 postgis24_96-utils.x86_64 postgis24_96-devel.x86_64 && \
    yum -y install https://yum.postgresql.org/9.6/redhat/rhel-6-x86_64/pgaudit_96-1.0.4-1.rhel6.x86_64.rpm && \
    yum clean all 


RUN echo 'postgres ALL=(ALL) ALL' >> /etc/sudoers
RUN chown -R postgres.postgres /var/lib/pgsql 
RUN sudo -u postgres /usr/pgsql-9.6/bin/initdb /var/lib/pgsql/9.6/data/

COPY ./postgresql.conf /var/lib/pgsql/9.6/data/postgresql.conf
COPY ./pg_hba.conf /var/lib/pgsql/9.6/data/pg_hba.conf
COPY ./pgsql_profile /var/lib/pgsql/.pgsql_profile
COPY ./dbdump /var/lib/pgsql/dbdump

EXPOSE 5432

RUN chmod +x /var/lib/pgsql/dbdump/dump_database_and_start.sh && \
    chown -R postgres.postgres /var/lib/pgsql && \
    sudo -u postgres /usr/pgsql-9.6/bin/pg_ctl start  -D /var/lib/pgsql/9.6/data -w && \
    sudo -u postgres /var/lib/pgsql/dbdump/dump_database_and_start.sh 
    
ENTRYPOINT ["sudo","-u","postgres","/usr/pgsql-9.6/bin/postgres","-D","/var/lib/pgsql/9.6/data","-p","5432"]
