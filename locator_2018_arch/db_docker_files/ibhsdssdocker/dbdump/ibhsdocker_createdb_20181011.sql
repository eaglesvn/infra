
create database ibhsdocker
    with owner = postgres
    encoding = 'UTF8'
    tablespace = pg_default
    lc_collate = 'en_US.UTF-8'
    lc_ctype = 'en_US.UTF-8'
    connection limit = -1;

alter database ibhsdocker set search_path = public,aims,brc,cams,cms,common,information_schema,isats,locator_etl,locator_etl_2017,OPS$CDS,pg_catalog,survey,tfa;
