
create database dssdocker
    with owner = postgres
    encoding = 'UTF8'
    tablespace = pg_default
    lc_collate = 'en_US.UTF-8'
    lc_ctype = 'en_US.UTF-8'
    connection limit = -1;

alter database ibhsdocker set search_path = public,common,information_schema,dss,teds,dss,cwms,chat,cld,dsscld,pg_catalog;

