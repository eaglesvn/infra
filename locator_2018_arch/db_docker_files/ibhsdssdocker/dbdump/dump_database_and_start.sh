#!/bin/sh
function create_db () {
whoami
echo "Status of Postgres:  "
/usr/pgsql-9.6/bin/pg_ctl status  -D /var/lib/pgsql/9.6/data 

echo " Running Postgres scripts "

psql -f /var/lib/pgsql/dbdump/dssdocker_globals_20181011.sql 
psql -f /var/lib/pgsql/dbdump/ibhsdocker_createdb_20181011.sql 
psql -f /var/lib/pgsql/dbdump/dssdocker_createdb_20181011.sql 
psql -f /var/lib/pgsql/dbdump/dssdocker_extensions_20181011.sql 
echo " Running IBHS dump "
pg_restore -d ibhsdocker /var/lib/pgsql/dbdump/ibhsdocker_FULL_20181011.dump 
echo " Running DSS dump "
pg_restore -d dssdocker /var/lib/pgsql/dbdump/dssdocker_FULL_20181011.dump 
}
create_db
ps -aef | grep postgres
/usr/pgsql-9.6/bin/pg_ctl stop -D /var/lib/pgsql/9.6/data -w
