#!/bin/bash

#REL_VERSION=$1

#ENV_NAME=$2
app_name=locator_play2_app

#SET=$3

function download_pkg {
cd /local/apps/bhsis

nexus_ID=admin
nexus_pass=YWRtaW4xMjM=
nexus_URL=https://bhsis02.eagletechva.com/repository/locator-prd-pkgs
nexus_password=$(echo -n "${nexus_pass}" | base64 --decode)
echo "/usr/bin/curl --remote-name -k --user "${nexus_user}:${nexus_password}" ${nexus_URL}/${app_name}-${REL_VERSION}_prd.zip"
/usr/bin/curl --remote-name -k --user "${nexus_ID}:${nexus_password}" ${nexus_URL}/${app_name}-${REL_VERSION}_prd.zip
cd /local/apps/bhsis && /usr/bin/unzip ${app_name}-${REL_VERSION}_prd.zip
rm -f /local/apps/bhsis/${app_name}
if [[ -f /local/apps/bhsis/${app_name} ]]
then
rm -f /local/apps/bhsis/${app_name}
else
:

fi 
ln -s /local/apps/bhsis/${app_name}-${REL_VERSION}  /local/apps/bhsis/${app_name}

}

function run_app {
if [[ $ENV_NAME == "" ]] || [[ $SET == "" ]]
then
echo "Please pass the arguments in their order"
exit 1
else

echo "ENV Name is $ENV_NAME and Set is $SET"

fi

if [[ $ENV_NAME == "TEST" ]]
then

env_file_name=ettst21.conf

elif [[  $ENV_NAME == "PROD" ]]
then

env_file_name=bhprd1.conf

else
echo "Environment does not match"

exit 1

fi

if [[ $SET = "SETA" ]]
then

/local/apps/bhsis/scripts/replace_variable.sh -p /local/apps/bhsis/scripts/${ENV_NAME}_${SET}.properties -d /local/apps/bhsis/${app_name}/

echo "172.20.0.10 ettstdb01" >> /etc/hosts
JVM_MEMORY_ARGS="-J-server -J-XX:+UseG1GC -J-Xms2G -J-Xmx2G"
echo "/local/apps/bhsis/${app_name}-${REL_VERSION}/bin/$app_name  $JVM_MEMORY_ARGS -Dconfig.resource=$env_file_name"
/bin/bash /local/apps/bhsis/${app_name}/bin/$app_name  $JVM_MEMORY_ARGS -Dconfig.resource=$env_file_name

elif [[ $SET = "SETB" ]]
then


/local/apps/bhsis/scripts/replace_variable.sh -p /local/apps/bhsis/scripts/${ENV_NAME}_${SET}.properties -d /local/apps/bhsis/${app_name}/

echo "172.20.0.10 ettstdb01" >> /etc/hosts
JVM_MEMORY_ARGS="-J-server -J-XX:+UseG1GC -J-Xms2G -J-Xmx2G"
echo "/local/apps/bhsis/${app_name}-${REL_VERSION}/bin/$app_name  $JVM_MEMORY_ARGS -Dconfig.resource=$env_file_name"
/bin/bash /local/apps/bhsis/${app_name}/bin/$app_name  $JVM_MEMORY_ARGS -Dconfig.resource=$env_file_name


else

echo "set value not found"

exit 1

fi 

}

download_pkg
run_app
