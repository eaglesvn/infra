#!/bin/bash
##Created by Pavan Kumar, this script download the package and then start the application.

APP_DIR_PATH=/local/apps/bhsis/locator_play2_app



#if [[ -d "${APP_DIR_PATH}" ]]

#then
#	svn info ${SVN_CO_URL}

#        if [[ $? -eq 0 ]]
#        then
# 	 	svn update -r ${SVN_REV} ${APP_DIR_PATH}  --username pavank --password $(echo -n "dXBoNjc4bXI=" | base64 --decode)  --no-auth-cache
		 
#        else
#	:

#	fi
	
#else	

#	cd /local/apps/bhsis
#	svn co  -r ${SVN_REV}  ${SVN_CO_URL}   --username pavank --password $(echo -n "dXBoNjc4bXI=" | base64 --decode)  --no-auth-cache
#fi

if [[ -d "${APP_DIR_PATH}" ]]
then

	cd /local/apps/bhsis/locator_play2_app
	/bin/yarn install && /bin/yarn build
	sbt clean 
	sbt dist

	cd /local/apps/bhsis/locator_play2_app/target/universal/
	unzip locator_play2_app*.zip 

	/bin/bash /local/apps/bhsis/locator_play2_app/target/universal/*/bin/locator_play2_app 

else 
	echo "Please checkout the application code and use the voume properly"

fi
