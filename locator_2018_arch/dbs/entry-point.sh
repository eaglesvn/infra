#!/bin/bash



chown -R postgres:postgres /var/lib/pgsql

sudo -u postgres /usr/pgsql-9.6/bin/initdb -D $PGDATA

cp /data/conf/postgresql.conf /var/lib/pgsql/9.6/data/postgresql.conf

cp /data/conf/pg_hba.conf  /var/lib/pgsql/9.6/data/pg_hba.conf

chmod -R 0700 /var/lib/pgsql

chown -R postgres:postgres /var/lib/pgsql

sudo -u postgres /usr/pgsql-9.6/bin/postmaster -D /var/lib/pgsql/9.6/data
#systemctl start postgresql-9.6
