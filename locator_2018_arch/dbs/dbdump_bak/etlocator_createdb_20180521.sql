--UPDATE pg_database SET datistemplate = FALSE WHERE datname = 'template1';
--DROP DATABASE template1;
--CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UNICODE';
--UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template1';
drop database etlocator  IF EXISTS ;
create database etlocator
    with owner = postgres
    encoding = 'UTF8'
    tablespace = pg_default
    lc_collate = 'en_US.UTF-8'
    lc_ctype = 'en_US.UTF-8'
    TEMPLATE = 'template0'
    connection limit = -1;

alter database etlocator set search_path = public,information_schema,pg_catalog;
