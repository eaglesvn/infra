drop database etlocator  IF EXISTS ;
create database etlocator
    with owner = postgres
    encoding = 'UTF8'
    tablespace = pg_default
    lc_collate = 'en_US.UTF-8'
    lc_ctype = 'en_US.UTF-8'
    connection limit = -1;

alter database etlocator set search_path = public,common,information_schema,locator,mhlocator,pg_catalog,ufds;
