#!/bin/bash
# set environment
JAVA_HOME=/opt/java/jdk_1.8
BASE_HOME=/local/apps/bhsis/cron-jira

#path
ENTS_HOME=$BASE_HOME/ents
CHAIN_HOME=$BASE_HOME/chains

CLASS_PATH=$CHAIN_HOME:$ENTS_HOME/action-chain-2018.02.21.jar:`echo $ENTS_HOME/lib/*.jar | tr ' ' ':'`
echo "Classpath: " $CLASS_PATH

#Error logging - at the shell level
# Note: we assume there is an ERROR IF the JobRunner output any messages...
DATE=`date +"%b %d, %Y"`
TIMESTAMP=`date +%Y-%b-%d-%H:%M:%S`
TEMP_LOG_FILE=/tmp/chain_jira_all_$TIMESTAMP.log

echo $TEMP_LOG_FILE

$JAVA_HOME/bin/java -cp $CLASS_PATH -Ddebug=false ExecuteChain $CHAIN_HOME/work_log_report/chain_email_work_log_report.conf > $TEMP_LOG_FILE 2>&1


errorLineCount=`grep Exception $TEMP_LOG_FILE | wc -l`
echo $errorLineCount error count
if [ $errorLineCount -gt 0 ]
then
        echo "sending error in mail ..."
        mail -s "Failed to execute Chain for email work log report on $DATE - `hostname`" ping.wang@eagletechva.com < $TEMP_LOG_FILE
else
        echo Job Run Successful
fi