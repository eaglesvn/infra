import prettytable


def table_of_deployed_to_test_tickets(jira_tickets, jira_url):
    # Return in a table format with jira tickets status deployed to test

    test_status_table = prettytable.PrettyTable()
    test_status_table.field_names = ['ID', 'Status', 'Issue Type', 'Peer Testing', 'Peer Tester']
    for jira_ticket in jira_tickets:
        if str(jira_ticket.fields.status) == 'Deployed to Test':
            jira_id = '<a href="' + jira_url + '/browse/' + jira_ticket.key + '" target="_blank">' + jira_ticket.key + '</a>'
            status = str(jira_ticket.fields.status)
            issue_type = str(jira_ticket.fields.issuetype)
            assignee = str(jira_ticket.fields.assignee)
            fix_version = str(jira_ticket.fields.fixVersions[0])
            try:
                peer_testing = str(jira_ticket.fields.customfield_10100[0])
            except:
                peer_testing = 'No Field'

            try:
                peer_tester = str(jira_ticket.fields.customfield_10101)
            except:
                peer_tester = 'No Field'
            test_status_table.add_row([jira_id, status, issue_type, peer_testing, peer_tester])
    return test_status_table


def table_of_release_tickets(jira_tickets, jira_url):
    '''Return the table of jira tickets in the release'''

    rel_table = prettytable.PrettyTable()

    rel_table.field_names = ['ID', 'Status', 'Issue Type', 'Peer Testing', 'Peer Tester']
    for jira_ticket in jira_tickets:
        jira_id = '<a href="' + jira_url + '/browse/' + jira_ticket.key + '" target="_blank"> ' + jira_ticket.key + '</a>'
        status = str(jira_ticket.fields.status)
        issue_type = str(jira_ticket.fields.issuetype)
        try:
            peer_testing = str(jira_ticket.fields.customfield_10100[0])
        except:
            peer_testing = 'No Field'

        try:
            peer_tester = str(jira_ticket.fields.customfield_10101)
        except:
            peer_tester = 'No Field'
        rel_table.add_row([jira_id, status, issue_type, peer_testing, peer_tester])
    return rel_table
