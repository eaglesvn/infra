from jira import JIRA
import argparse
import report
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import create_conf_rel_page
import datetime
import pytz
import re
from cryptography.fernet import Fernet
import logging

'''This script is main and send the JIRA report in email and call the 
create conf release to create the release sheet in confluence'''

parser = argparse.ArgumentParser(description='Pass the required arguments')
parser.add_argument('-c', '--component', help='Jira Component Name', required=True)
parser.add_argument('-r', '--release_version', help='Jira release version number', required=True)
parser.add_argument('-e', '--email_addresses', help='Pass email address in quote with command delimiter')
parser.add_argument('-rev', '--git_hash', help='SVN Revision Number')
parser.add_argument('-id', '--build_id', help='Jenkins Build ID')
parser.add_argument('-u', '--build_user_name', help='Jenkins Build User')
parser.add_argument('-b', '--git_branch', help='SVN Path')
parser.add_argument('-t', '--rel_type', help='Release type APP, CRON or others')
parser.add_argument('-l', '--log_file', help='Enter the log file name where you want to save the log '
                                             'file,by default it goes to /tmp directory')

args = parser.parse_args()

if args.component:
    component = args.component

if args.release_version:
    fix_version = args.release_version

if args.git_hash:
    git_hash = args.git_hash

if args.build_id:
    build_id = args.build_id
else:
    build_id = 'Not able to get Build ID from Jenkins'

if args.build_user_name:
    build_user_name = args.build_user_name
else:
    build_user_name = 'Not able to get Build User from Jenkins'

if args.git_branch:
    git_branch = args.git_branch

if args.rel_type:
    rel_type = args.rel_type

if args.email_addresses:
    toaddrs = args.email_addresses.replace(" ", "").split(',')

else:
    toaddrs = ['pavan.kumar@eagletechva.com']

if args.log_file:
    log_file = args.log_file
else:
    log_file = ('/tmp/' + component + '_jira_release_version.log')



#log_file = 'jira.log'


logging.basicConfig(filename=log_file, filemode='w',
                    format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

project = 'BIT'
resolution = 'Unresolved'

'''
component = 'DASIS-HOME'
fix_version = 'DASIS-HOME_1.2.112'
svn_rev = '35'
svn_path = 'my branch'
build_id = 34
build_user_name = 'pavank'
rel_type = 'Web Application'
'''
fromaddr = "pavan.kumar@eagletechva.com"
#toaddrs = ['pavan.kumar@eagletechva.com']

logging.info('Component args: '.format(component))
logging.info('Release Version args: '.format(fix_version))
logging.info('Git Commit Hash args: '.format(git_hash))
logging.info('Build ID args: '.format(build_id))
logging.info('Build User Name args: '.format(build_user_name))
logging.info('Git Branch args: '.format(git_branch))
logging.info('Release Type: '.format(rel_type))


key_file = 'jira.key'
with open(key_file, 'rb') as jk_r:
    jk_k = jk_r.read()

jira_eny_pass = b'gAAAAABdfYpy7c1AssXPTqXmK8xYzMoOmWVxoISRKceF9tP9FEzFKpKprcq6gT8K4hTptmRdVJv1RYFlmNDNpDbkMDH_8reDKg=='

cryp_obj = Fernet(jk_k)


conf_rel_url = create_conf_rel_page.conf_url + '/display/' + create_conf_rel_page.space + '/' + fix_version + '_release_sheet'
jira_user = 'pavank'
jira_password = cryp_obj.decrypt(jira_eny_pass).decode('utf-8')
jira_url = 'http://etinf03.eagletechva.com/jira'
options = {'server': jira_url}


# JIRA transition ID and status: Do not change
transition_id = '711'
current_jira_status = 'Development Completed'
report_status = 'Deployed to Test'


jira = JIRA(options, basic_auth=(jira_user, jira_password))
jira_tickets = jira.search_issues('project = {} AND resolution = {} AND fixVersion =  '
                                   '{}  AND component = {} ORDER BY priority DESC, updated'
                                   ' DESC'.format(project, resolution, fix_version, component))


def transition_move_status():
    '''Move the JIRA ticket status from development complete to deployed to test
     transition id is 711 for deployed to test'''

    for jira_ticket in jira_tickets:
        if str(jira_ticket.fields.status) == current_jira_status:
            jira.transition_issue(jira_ticket.key, transition_id)

transition_move_status()

jira2 = JIRA(options, basic_auth=(jira_user, jira_password))
jira2_tickets = jira2.search_issues('project = {} AND resolution = {} AND fixVersion =  '
                                    '{}  AND component = {} ORDER BY priority DESC, updated'
                                    ' DESC'.format(project, resolution, fix_version, component))


report_jira_dp_test = report.table_of_deployed_to_test_tickets(jira2_tickets, jira_url).get_html_string()
report_all_jira_table = report.table_of_release_tickets(jira2_tickets, jira_url).get_html_string()

less_than = re.compile(r'\&lt;')
greater_than = re.compile(r'\&gt;')
re_quote = re.compile(r'&quot;')

report_jira_dp_test = re.sub(less_than, '<', report_jira_dp_test)
report_jira_dp_test = re.sub(greater_than, ">", report_jira_dp_test)
report_jira_dp_test = re.sub(re_quote, '"', report_jira_dp_test)

report_all_jira_table = re.sub(less_than, '<', report_all_jira_table)
report_all_jira_table = re.sub(greater_than, ">", report_all_jira_table)

#deployment_date = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

deployment_date_utc = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
nyc_timezone = pytz.timezone('America/New_York')
deployment_date = nyc_timezone.normalize(deployment_date_utc.astimezone(nyc_timezone)).strftime("%Y-%m-%d_%H:%M:%S")

with open('app_rel_info.html', 'r') as fh:
    rel_html = fh.read()

rel_info = rel_html.format(build_user_name, build_id, git_hash, git_branch, rel_type, deployment_date)

logging.info('Release info:'.format(rel_info))

conf_jira_dp_table = rel_info + report_jira_dp_test


try:
    confluence_page_parent_id = create_conf_rel_page.get_or_create_parent_id(component, fix_version)

except Exception as confluence_error:
    print('Can not get the parent page id')
    logging.error('Not able to get the parent page id: '.format(confluence_error))

else:
    print("Page ID:", confluence_page_parent_id)
    logging.info('Confluence Page Patent ID: '.format(confluence_page_parent_id))

    create_conf_rel_page.create_or_update_release_sheet(confluence_page_parent_id,
                                                        component, fix_version,
                                                        conf_jira_dp_table)


# noinspection PyStringFormat,PyStringFormat
html1 = ('''\
<html>
<head>
<style>
table, td, th {
  border: 1px solid black;
}

table {
  border-collapse: collapse;
  width: 100%;
}

td, th {
  text-align: center;
}
</style>
</head>

''')
html2 = '''
<body>
<h4> Test Deployment for {2} and Release Version {3}: </h4>
<p><strong>Release Sheet Link:</strong> {0} </p>
{1}

<h4> 1. JIRA tickets status in 'Deployed to Test' or status has changed to 
    'Deployed to Test' for {2} and release version is {3}:</h4>
{4}
</br>
<h4> 2. ALL the JIRA tickets in {2} and release version is {3}:</h4>
{5}
</body>
</html>
'''.format(conf_rel_url, rel_info, component, fix_version, report_jira_dp_test, report_all_jira_table)

html = (html1 + html2)

msg = MIMEMultipart()
msg = MIMEText(html, "html")

msg['From'] = fromaddr
msg['To'] = ", ".join(toaddrs)
msg['Subject'] = "Test Deployment for {0} and Release Version {1} JIRA tickets".format(component, fix_version)

server = smtplib.SMTP('eagletechva-com.mail.protection.outlook.com')
server.sendmail(fromaddr, toaddrs, msg.as_string())
server.quit()

logging.info('HTML Page of TEST JIRA Tickets: '.format(html))
