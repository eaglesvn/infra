#!/bin/sh
function create_db () {
whoami
echo "Status of Postgres:  "
pg_ctl status  -D /var/lib/postgresql/data 
echo " Running Postgres scripts "
psql -U postgres -f /var/lib/postgresql/dbdump/bhsislocator_globals_20190302.sql 
psql -U postgres -f /var/lib/postgresql/dbdump/bhsislocator_createdb_20190302.sql
psql -U postgres -f /var/lib/postgresql/dbdump/bhsislocator_extensions_20190302.sql 
pg_restore -U postgres -d bhsislocator  /var/lib/postgresql/dbdump/bhsislocator_FULL_20190302.dump 
}
create_db
ps -aef | grep postgres
pg_ctl stop -D /var/lib/postgresql/data -w
