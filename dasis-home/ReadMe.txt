##

##How to build the docker images.

## Dasis home page web image. 

cd dasis-home-web
docker build -t dasis-home-web:v1.0.0 .
 
#Build the play application docker image:
#Go to the dasis-home-play from current directory and run the folowing command. 

cd dasis-home-play
docker build -t dasis-home-play:v1.0.0 .

#Build the Nginx proxy image.
#Go to the dasis-home-prx from the current directoy and come back to dasis-home direcotry if you are on

cd dasis-home-prx 
docker build -t dasis-home-prx:v1.0.0

##After building the images, you can run the docker-compose commands. 
#How to run the docker-compose, option -d run the docker compose in background
#Run below command if you in the dasis-home directory

docker-compose  up -d  

#Run below command if you are not in the dasis-home dirctory
#For example, docker-compose -f <<directory path>>/<<docker compose file>> up -d

#docker-compose -f ./dasis-home/docker-compose.yaml up -d

#How to stop the docker-compose 

#docker-compose stop

#or do this way if you are not in the dasis-home direcotry 
#for example, docker-compose -f <<directory path>>/<<docker compose file>> stop

#docker-compose -f ./dasis-home/docker-compose.yaml stop

#How to remove the docker-compose containers 

#docker-compose rm

#or do this way if you are not in the dasis-home direcotry
#for example, docker-compose -f <<directory path>>/<<docker compose file>> rm


