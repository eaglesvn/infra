-- Update passwords
ALTER ROLE dea_app WITH password 'd34@$b$b';
ALTER ROLE dss_readonly WITH password 'd33$Read';

-- Grant usages
grant USAGE on SCHEMA dea to dea;
grant USAGE on SCHEMA dea to dea_app;
grant usage on SCHEMA dea to dea_read_only 

grant USAGE on SCHEMA cld to cld;
grant USAGE on SCHEMA cld to dsscld;
grant USAGE on SCHEMA cld to dsscld_app;

grant usage on SCHEMA dss to dss ;
grant usage on SCHEMA dss to teds ;
grant usage on SCHEMA dss to dea ;
grant usage on SCHEMA dss to dss_app ;
grant USAGE on SCHEMA dss to dss_readonly;

grant usage ON SCHEMA dsscld to dsscld_app ;

grant USAGE on SCHEMA teds to teds ;
grant usage on schema teds to dss;
grant usage on SCHEMA teds to dss_read_only;
grant usage on SCHEMA teds to dss_app;
grant usage on SCHEMA teds to dss_readonly;

grant usage on SCHEMA common to common;
grant usage on SCHEMA common to dss;
grant usage on SCHEMA common to dss_read_only;
grant usage on SCHEMA common to dss_app;
grant usage on SCHEMA common to dss_readonly;
grant usage on SCHEMA common to cld;
grant usage on SCHEMA common to dsscld;
grant usage on SCHEMA common to dsscld_app;

-- Grant creates
grant CREATE on SCHEMA dea to dea_app;
grant CREATE on SCHEMA cld to dsscld_app;
grant CREATE on SCHEMA dss to dss_app;
grant create on SCHEMA dsscld to dsscld_app ;
grant create on SCHEMA teds to dss;


-- Grant PRIVILEGES
--CLD
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA cld TO cld;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA cld TO cld;
GRANT SELECT ON ALL TABLES IN SCHEMA cld TO dsscld;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA cld TO dsscld_app;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA cld TO dsscld_app;


--DEA
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA dea TO dea;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA dea TO dea;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA dea TO dea_app;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA dea TO dea_app;
GRANT SELECT ON ALL TABLES IN SCHEMA dea TO dea_read_only;

--DSS
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA dss TO dss;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA dss TO dss;
GRANT SELECT ON ALL TABLES IN SCHEMA dss TO teds;
GRANT SELECT ON ALL TABLES IN SCHEMA dss TO dea;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA dss TO dss_app;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA dss TO dss_app;
GRANT SELECT ON ALL TABLES IN SCHEMA dss TO dss_readonly;

--DSSCLD
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA dsscld TO dsscld;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA dsscld TO dsscld;
GRANT SELECT ON ALL TABLES IN SCHEMA dsscld TO cld;
GRANT SELECT ON ALL TABLES IN SCHEMA dsscld TO dsscld_app;


--TEDS
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA teds TO teds;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA teds TO teds;
GRANT SELECT ON ALL TABLES IN SCHEMA teds TO dss;
GRANT SELECT ON ALL TABLES IN SCHEMA teds TO dss_read_only;
GRANT SELECT ON ALL TABLES IN SCHEMA teds TO dss_app;
GRANT SELECT ON ALL TABLES IN SCHEMA teds TO dss_readonly;


-- REMOVE OLD DATA
delete from aims_user ;
delete from batch_detail_extract ;
delete from batch_info;
delete from batch_info_back ;
delete from month_wise_report_mh ;
delete from prodstat_batch_details;
delete from prodstat_batch_details_mh ;
delete from prodstat_batch_info ;
delete from report_config ;
