## Create rpmbuild directories
mkdir -p ./rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

## Create macros file and define required vars
cat <<EOF >~/.rpmmacros
%_topdir   `pwd`/rpmbuild
%_tmppath  %{_topdir}/tmp
EOF

## Get current rpm version
CURRENT_VERSION=`grep "Version:*" dea_db.spec`
echo Current RPM ${CURRENT_VERSION}

## Prompt user for new rpm version and replace in file
read -p "Enter new RPM version: " VERSION
sed -i.bak "s/^Version:.*/Version:        ${VERSION}/" dea_db.spec && rm dea_db.spec.bak

# Copy dea specfile into SPECS directory
cp dea_db.spec ./rpmbuild/SPECS/dea_db.spec

## Perform rpmbuild to generate .rpm file
rpmbuild -v -bb ./rpmbuild/SPECS/dea_db.spec

## Upload RPM to Nexus repo
## DONT FORGET TO BUMP THE VERSION NUMBER IN THE spec file.
echo 'Enter your svn information to upload the rpm to the nexus repo'
read -p 'svn username: ' USERNAME
read -sp 'svn password: ' PASSWORD

echo "Updating svn repo spec file to have newest version"
echo ""
svn commit dea_db.spec -m "BIT - 3850 - INFRA - Create rpms for ibhs,dss,devs,locator,dsscld,dea

Updated rpm version for dea database"

curl -v --user ${USERNAME}:${PASSWORD} --upload-file ./rpmbuild/RPMS/x86_64/dea_db-${VERSION}-0.el7.x86_64.rpm https://bhsis02.eagletechva.com/repository/eagle-yum-repo/dea_db-${VERSION}-0.el7.centos.x86_64.rpm
