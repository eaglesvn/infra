In order to update the RPM:
1. Launch the DB container (optionally running desired delta scripts)
2. Make any other desired changes
3. docker exec -it into the container
4. svn checkout this directory to the container
5. run ./create_rpm.sh 
6. specify the version number (the script will automatically commit a
change to the .spec file)