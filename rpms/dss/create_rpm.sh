
## Create rpmbuild directories
mkdir -p ./rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

## Create macros file and define required vars
cat <<EOF >~/.rpmmacros
%_topdir   `pwd`/rpmbuild
%_tmppath  %{_topdir}/tmp
EOF

## Get current rpm version
CURRENT_VERSION=`grep "Version:*" dss_db.spec`
echo Current RPM ${CURRENT_VERSION}

## Prompt user for new rpm version and replace in file
read -p "Enter new RPM version: " VERSION
sed -i.bak "s/^Version:.*/Version:        ${VERSION}/" dss_db.spec && rm dss_db.spec.bak

# Copy devs specfile into SPECS directory
cp dss_db.spec ./rpmbuild/SPECS/dss_db.spec

## Perform rpmbuild to generate .rpm file
rpmbuild -v -bb ./rpmbuild/SPECS/dss_db.spec

## Upload RPM to Nexus repo
echo 'Enter your svn information to upload the rpm to the nexus repo'
read -p 'svn username: ' USERNAME
read -sp 'svn password: ' PASSWORD

echo "Updating svn repo spec file to have newest version"
echo ""
svn commit dss_db.spec -m "BIT - 3850 - INFRA - Create rpms for ibhs,dss,devs,locator,dsscld

Updated rpm version for dss_db repo"

curl -v --user ${USERNAME}:${PASSWORD} --upload-file ./rpmbuild/RPMS/x86_64/dss_db-${VERSION}-0.el7.x86_64.rpm https://bhsis02.eagletechva.com/repository/eagle-yum-repo/dss_db-${VERSION}-0.el7.centos.x86_64.rpm
