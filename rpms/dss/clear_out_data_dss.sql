--Run entire script as postgres user
TRUNCATE dss.audit_trail_file,
  dss.comment_upload_file,
  dss.audit_trail_submission,
  teds.discharge_file,
  teds.admission_file,
  teds.treatment,
  teds.treatment_dropped_records,
  teds.discharge,
  teds.discharge_dropped_records,
  teds.submission,
  dss.upload_file,
  dss.aims_user,
  dss.feedback,
  dss.audit_trail_crosswalk,
  dss.mapping,
  dss.audit_trail_validation_rule,
  dss.server_notification,
  cwms.crosswalks,
  cwms.state_forms,
  cwms.state_items,
  cwms.state_item_categories,
  cwms.state_teds_category_relationsh,
  cwms.state_teds_relationships;

ALTER SEQUENCE dss.aims_user_seq RESTART;
ALTER SEQUENCE dss.audit_trail_crosswalk_seq RESTART;
ALTER SEQUENCE dss.audit_trail_file_seq RESTART;
ALTER SEQUENCE dss.audit_trail_seq RESTART;
ALTER SEQUENCE dss.audit_trail_submissions_seq RESTART;
ALTER SEQUENCE dss.comment_upload_file_id_seq RESTART;
ALTER SEQUENCE dss.feedback_id_seq RESTART;
ALTER SEQUENCE dss.mapping_seq RESTART;
ALTER SEQUENCE dss.record_field_value_seq RESTART;
ALTER SEQUENCE dss.server_notification_seq RESTART;
ALTER SEQUENCE dss.state_user_audit_trail_seq RESTART;
ALTER SEQUENCE dss.upload_file_id_seq RESTART;

ALTER SEQUENCE cwms.crosswalk_seq RESTART;
ALTER SEQUENCE cwms.state_cat_seq RESTART;
ALTER SEQUENCE cwms.state_form_seq RESTART;
ALTER SEQUENCE cwms.state_items_seq RESTART;
ALTER SEQUENCE cwms.state_teds_cat_relat_seq RESTART;
ALTER SEQUENCE cwms.state_teds_relat_seq RESTART;

--Drop unused sequences, views, tables
DROP FUNCTION IF EXISTS dss.load_admission_file(p_submission_id INTEGER);
DROP FUNCTION IF EXISTS dss.load_discharge_file(p_submission_id INTEGER);

DROP SEQUENCE IF EXISTS cwms.comments_seq,
  dss.allowable_incoming_ip_id_seq,
  dss.validation_function_seq,
  dss.submission_seq,
  dss.authentication_log_id_seq,
  dss.mapping_revision_seq,
  dss.state_user_audit_trail_seq;

DROP VIEW IF EXISTS cwms.comments_teds_v;
DROP TABLE IF EXISTS dss.allowable_incoming_ip,
  dss.authentication_log,
  dss.junk,
  dss.junk22,
  dss.legacy_upload_file,
  dss.legacy_validation_function,
  dss.legacy_validation_rule,
  dss.mapping_revision,
  teds.code_values,
  teds.edit_list,
  teds.monthly_rpt_table1,
  teds.monthly_rpt_table2,
  teds.monthly_rpt_table3,
  teds.quarterly_rpt_table4,
  teds.state_submissions,
  teds.submission_actions,
  teds.submission_error_fields,
  teds.submission_error_keys,
  teds.submission_status_log,
  teds.validation_codes,
  teds.vw_date_ranges,
  teds.vw_prod_counts_,
  cwms.comments,
  cwms.comments_bk102108,
  cwms.crosswalks_bk102108,
  cwms.users,
  cwms.teds_item_flex_assignments;

--TODO teds_code_value and teds_field still have usages in dss (crosswalk-related functionality). This is duplicated data. The usages should be removed and then these tables should be dropped
--TODO xref_submission_type has a model class in dss. This is duplicate data. Need to determine and remove usages of this class, and drop the xref_submission_type table

