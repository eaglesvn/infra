## Create rpmbuild directories
mkdir -p ./rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

# Copy devs specfile into SPECS directory
cp devs_db.spec ./rpmbuild/SPECS/devs_db.spec

## Create macros file and define required vars
cat <<EOF >~/.rpmmacros
%_topdir   `pwd`/rpmbuild
%_tmppath  %{_topdir}/tmp
EOF

## Perform rpmbuild to generate .rpm file
rpmbuild -v -bb ./rpmbuild/SPECS/devs_db.spec

## Upload RPM to Nexus repo
## DONT FORGET TO BUMP THE VERSION NUMBER IN THE spec file.
echo 'Enter your svn information to upload the rpm to the nexus repo'
read -p 'svn username: ' USERNAME
read -sp 'svn password: ' PASSWORD

VERSION=1.0.2
curl -v --user ${USERNAME}:${PASSWORD} --upload-file ./rpmbuild/RPMS/x86_64/devs_db-${VERSION}-0.el7.centos.x86_64.rpm http://etinf21.eagletechva.com:8081/repository/eagle-yum-repo/devs_db-${VERSION}-0.el7.centos.x86_64.rpm
