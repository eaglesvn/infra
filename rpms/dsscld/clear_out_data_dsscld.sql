--Instead of running as users specified below, you can this truncate command as postgres
TRUNCATE dsscld.audit_trail_file,
  dsscld.comment_upload_file,
  dsscld.audit_trail_submission,
  dsscld.server_notification,
  dsscld.feedback,
  cld.client_readmission_flat_transaction,
  cld.client_basic_flat_transaction,
  cld.client_basic_flat,
  cld.client_basic_flat_dropped_records,
  cld.client_readmission_flat,
  cld.client_readmission_flat_dropped_records,
  cld.submission,
  dsscld.upload_file,
  cld.acceptance_rpt_data,
  cld.acceptance_rpt_job,
  dsscld.aims_user;

--Set sequences to 1
ALTER SEQUENCE cld.acceptance_rpt_data_id_seq RESTART;
ALTER SEQUENCE cld.acceptance_rpt_job_id_seq RESTART;
ALTER SEQUENCE cld.client_basic_flat_id_seq RESTART;
ALTER SEQUENCE cld.client_basic_flat_transaction_seq RESTART;
ALTER SEQUENCE cld.client_readmission_flat_id_seq RESTART;
ALTER SEQUENCE cld.client_readmission_flat_transaction_seq RESTART;
ALTER SEQUENCE cld.submission_id_seq RESTART;

ALTER SEQUENCE dsscld.audit_trail_file_seq RESTART;
ALTER SEQUENCE dsscld.audit_trail_submission_seq RESTART;
ALTER SEQUENCE dsscld.comment_upload_file_id_seq RESTART;
ALTER SEQUENCE dsscld.feedback_id_seq RESTART;
ALTER SEQUENCE dsscld.server_notification_seq RESTART;
ALTER SEQUENCE dsscld.upload_file_id_seq RESTART;

--Tables not used/needed for application
DROP TABLE IF EXISTS
  cld.bci_dropped_records,
  cld.cl_prod,
  cld.cld_urs_code_mapping,
  cld.client_record,
  cld.error_code,
  cld.error_count,
  cld.field_error_log,
  cld.field_value,
  cld.file_process_log,
  cld.icd10_codecrosswalk,
  cld.icd9_diagnostic_group1_codes,
  cld.icd9_diagnostic_group2_codes,
  cld.icddiagnosis_reference,
  cld.lkp_record,
  cld.na_field_value,
  cld.pav_field_error_log,
  cld.record_field,
  cld.record_type,
  cld.records_tracking,
  cld.rs_edit_list,
  cld.rs_error_log,
  cld.shr_dropped_records,
  cld.state_error_log,
  cld.state_notes,
  cld.state_nri_mapping,
  cld.states,
  cld.user_states;



-- --RUN AS dsscld
-- DELETE FROM dsscld.audit_trail_file;
-- DELETE FROM dsscld.comment_upload_file;
-- DELETE FROM dsscld.audit_trail_submission;
--
-- --RUN AS cld
-- DELETE FROM cld.client_readmission_flat_transaction;
-- DELETE FROM cld.client_basic_flat_transaction;
-- DELETE FROM cld.client_basic_flat;
-- DELETE FROM cld.client_basic_flat_dropped_records;
-- DELETE FROM cld.client_readmission_flat;
-- DELETE FROM cld.client_readmission_flat_dropped_records;
-- DELETE FROM cld.submission;
--
--
-- --RUN AS dsscld
-- DELETE FROM dsscld.upload_file;
--



