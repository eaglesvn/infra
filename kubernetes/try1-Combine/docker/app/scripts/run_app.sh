#!/bin/bash
##Created by Pavan Kumar

SCRIPT_PATH=/data/scripts
APP_PATH=/local/apps/bhsis

function run_app {
if [[ $ENV_NAME == "" ]] || [[ $SET == "" ]]
then
echo "Please pass the arguments in their order"
exit 1
else

echo "ENV Name is $ENV_NAME and Set is $SET"

fi

if [[ $ENV_NAME == "TEST" ]]
then

env_file_name=ettst21.conf

elif [[  $ENV_NAME == "PROD" ]]
then

env_file_name=bhprd1.conf

else
echo "Environment does not match"

exit 1

fi

if [[ $SET = "SETA" ]]
then

${SCRIPT_PATH}/replace_variable.sh -p ${SCRIPT_PATH}/${ENV_NAME}_${SET}.properties -d ${APP_PATH}/${APP_NAME}/

JVM_MEMORY_ARGS="-J-server -J-XX:+UseG1GC -J-Xms2G -J-Xmx2G"
echo "${APP_PATH}/${APP_NAME}-${REL_VERSION}/bin/$APP_NAME  $JVM_MEMORY_ARGS -Dconfig.resource=$env_file_name"
/bin/bash ${APP_PATH}/${APP_NAME}/bin/$APP_NAME  $JVM_MEMORY_ARGS -Dconfig.resource=$env_file_name

elif [[ $SET = "SETB" ]]
then

${SCRIPT_PATH}/replace_variable.sh -p ${SCRIPT_PATH}/${ENV_NAME}_${SET}.properties -d ${APP_PATH}/${APP_NAME}/

JVM_MEMORY_ARGS="-J-server -J-XX:+UseG1GC -J-Xms2G -J-Xmx2G"
echo "${APP_PATH}/${APP_NAME}-${REL_VERSION}/bin/$APP_NAME  $JVM_MEMORY_ARGS -Dconfig.resource=$env_file_name"
/bin/bash ${APP_PATH}/${APP_NAME}/bin/$APP_NAME  $JVM_MEMORY_ARGS -Dconfig.resource=$env_file_name


else

echo "set value not found"

exit 1

fi 

}

run_app
