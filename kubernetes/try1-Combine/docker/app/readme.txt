##Purpose of this file is to provide the information how to run the basic docker command or provide the details on the image.


## How to build  the docker image on your local system
## You can use the . (dot) or path to dockerfile if you are not in the current directory.
#docker build --rm -t <image_name>:<tag_name/image version> .

docker build --rm -t locator:05 .

## how to login with private docker registry

#docker login <private docker registry  servername>, it will prompt for your the user name and password.

docker login bhsis02.eagletechva.com

## How to push the docker docker image to private docker resistry

## If you do not have the tag with private registry server name then you need to create the tag first.

# docker tag <<image id/image name with it's tag/version nuuber>> <private docker registry  servername>/<<image name that you want give>><<Tag or Version number that you want to give>>

docker tag locator_app:05 bhsis02.eagletechva.com/locator_app:05

## Push to the private docker registry 

docker push bhsis02.eagletechva.com/locator_app:05

## How to pull the image from the private docker registry

## docker pull <private docker registry  servername>/<<image name that you want give>><<Tag or Version number that you want to give>> 

docker pull bhsis02.eagletechva.com/locator_app:05

##Continue... 
