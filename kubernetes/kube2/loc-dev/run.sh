#!/bin/bash
## Create by Pavan Kumar
## This script just run the kubernete locator development commands, it will improve by the time.
## I have not put conditions and status check. Pleae contact Pavan Kumar if you need the help. 

##Running the Docker Registry yml file. 
kubectl apply -f docker-secret.yaml

## Creating the DB Persistent Volume
kubectl apply -f ./db/loc-dev-db-pv.yml

## Creating the DB Persistent Volume Claim
kubectl apply -f ./db/loc-dev-db-pvc.yml

##Creating the DB service
kubectl apply -f ./db/loc-dev-db-svc.yml

##Running the DB pod
kubectl apply -f ./db/loc-dev-db.yml

##Running the DB Dump script. 
kubectl exec loc-dev-db -- bash /data/dbdump/dump_etlocator_db.sh


### Running the applications. 

## Creating the app Persistent Volume 1
kubectl apply -f ./app/loc-dev-app-pv.yml

## Creating the app Persistent Volume 2
kubectl apply -f ./app/loc-dev-app-pv2.yml

## Creating the app Persistent Volume Claim
kubectl apply -f ./app/loc-dev-app-pvc.yml

## Creating the app Persistent Volume Claim 2
kubectl apply -f ./app/loc-dev-app-pvc2.yml

##Creating the APP  service
kubectl apply -f ./app/loc-dev-app-svc.yml
##Running the app Pod
kubectl apply -f ./app/loc-dev-app.yml




	


