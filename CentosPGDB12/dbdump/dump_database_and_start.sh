#!/bin/sh
function create_db () {
whoami
echo "Status of Postgres:  "
/usr/pgsql-12/bin/pg_ctl status  -D /var/lib/pgsql/12/data 

echo " Running Postgres scripts "

psql -f /var/lib/pgsql/dbdump/cr_db_users.sql 
echo " Running IBHS dump "
pg_restore -d ibhs /var/lib/pgsql/dbdump/ibhs.dump 

echo " Restored IBHS data  "
}
create_db
echo " Stopping the database using pg_ctl"
/usr/pgsql-12/bin/pg_ctl stop -D /var/lib/pgsql/12/data -w
