#!/bin/bash
##Created by Pavan Kumar
##This script run the docker run command to generate the CSV file
DOCKER_VOLUME=/bk3/locator_R_analysis/docker_volume
IMAGE_VOLUME_PATH=/local
IMAGE_NAME=bhsis02.eagletechva.com/r-base:3.5_v3
CONTAINER_NAME=R_CSV
SCRIPT_NAME=/local/rscripts/process-logs/process-locator-log.r
LOG_FILE_DATE=$(date +%Y%m%d)
RUNNING_CONTAINER_NAME=$(docker ps -a |grep R_CSV |awk  '{print $NF}')


DATE=${1:-$LOG_FILE_DATE}

LOG_FILE_NAME=/local/nginx_log_files/access.log-${DATE}
docker stop ${RUNNING_CONTAINER_NAME}

docker rm ${RUNNING_CONTAINER_NAME} 

docker run -i --name ${CONTAINER_NAME} -v ${DOCKER_VOLUME}:${IMAGE_VOLUME_PATH} ${IMAGE_NAME} ${SCRIPT_NAME} ${LOG_FILE_NAME}
