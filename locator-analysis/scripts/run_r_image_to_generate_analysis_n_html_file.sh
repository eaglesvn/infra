#!/bin/bash
##Created by Pavan Kumar
##This script run the docker run command to generate the HTML file

echo  "Usage:
-------------------
First Argument can be: 'Daily, Weekly or Monthly'
Second Argument can be: Date 'Year-Month-Day' like 2018-10-25
For example: For example: ./run_r_image_to_generate_analysis_n_html_file.sh Daily 2018-10-25
Daily:
--------------------
By default this script run for Daily analysis with yesterday's date.
However, you can pass first argument as 'Daily' and second argument of a Date 'Year-Month-Day', 
to run for a specific date.
For example: ./run_r_image_to_generate_analysis_n_html_file.sh Daily 2018-10-25
--------------------
Weekly:
To run the script for weekly analysis, pass the first argument as 'Weekly' and Date. 
You need to pass the Date argument only when you want the analysis from a specfic date.
For example: Running for a current week
./run_r_image_to_generate_analysis_n_html_file.sh Weekly
Running for a specific date
./run_r_image_to_generate_analysis_n_html_file.sh Weekly 2018-10-25
-------------------
Monthly:
To run the script for monthly analysis, pass the first argument as 'Monthly' and Date. 
By default it selects the yesterday's date. 
You need to pass the Date argument only when you want the analysis from a specfic date.
For example: Running for a current Month.
./run_r_image_to_generate_analysis_n_html_file.sh Monthly
Running for a specific date
./run_r_image_to_generate_analysis_n_html_file.sh Monthly 2018-10-25
------------------

"

DOCKER_VOLUME=/bk3/locator_R_analysis/docker_volume
IMAGE_VOLUME_PATH=/local
IMAGE_NAME=bhsis02.eagletechva.com/r-base:3.5_v3
CONTAINER_NAME=R_analysis
SCRIPT_NAME=/local/rscripts/process-summary/render-locator-summary.r
TODAY_DATE=$(date +%Y-%m-%d)
CSV_FILES_DIR=/local/CSV_files/
RUNNING_CONTAINER_NAME=$(docker ps -a |grep ${CONTAINER_NAME} |awk  '{print $NF}')
#HTML_FILE=/local/html/
R_SCRIPT_DIR=/local/rscripts/process-summary/
##Filter to remove the css, js, png files
BOOLEAN=true
docker stop ${RUNNING_CONTAINER_NAME}

docker rm ${RUNNING_CONTAINER_NAME} 

RUN_DAY=${1:-Daily}

DATE=${2:-$TODAY_DATE}

echo "Today's DATE is $DATE"
echo "Run time $RUN_DAY"

if [[ ${RUN_DAY} == 'Daily' ]]
then
	if [[ $2 == '' ]] 
	then 
		DATE=$(date --date="yesterday" +%Y-%m-%d)
                echo "Date is now $DATE"
	else
		echo "Date is $DATE"
	fi
	if [[ ! -d ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE} ]]
	then
		mkdir -p ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}
		HTML_FILE=/local/html/${RUN_DAY}/${DATE}/

	else
	        mv ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE} ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}.bak
                mkdir -p ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}
                HTML_FILE=/local/html/${RUN_DAY}/${DATE}/

	fi
elif [[ ${RUN_DAY} == 'Weekly' ]]
then
        if [[ ! -d ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE} ]]
        then
                mkdir -p ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}
                HTML_FILE=/local/html/${RUN_DAY}/${DATE}/
        else
                mv ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE} ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}.bak
                mkdir -p ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}
                HTML_FILE=/local/html/${RUN_DAY}/${DATE}/
        fi

elif [[ ${RUN_DAY} == 'Monthly' ]]
then

        if [[ $2 == '' ]]
        then 
                DATE=$(date --date="yesterday" +%Y-%m-%d)
                echo "Date is now $DATE"
        else
                echo "Date is $DATE"
        fi

        if [[ ! -d ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE} ]]
        then

                mkdir -p ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}
                HTML_FILE=/local/html/${RUN_DAY}/${DATE}/
        else

                mv ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE} ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}.bak
                mkdir -p ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}/
                HTML_FILE=/local/html/${RUN_DAY}/${DATE}/
        fi

else
	echo "Running the script as default value daily with yesterday' date"

        DATE=$(date --date="yesterday" +%Y-%m-%d)
        echo "Date is now $DATE"


        if [[ ! -d ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE} ]]
        then
                mkdir -p ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}/
                HTML_FILE=/local/html/${RUN_DAY}/${DATE}/
        else
                mv ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE} ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}.bak
                mkdir -p ${DOCKER_VOLUME}/html/${RUN_DAY}/${DATE}
                HTML_FILE=/local/html/${RUN_DAY}/${DATE}/
        fi


fi
echo "docker run -i --name ${CONTAINER_NAME} --workdir ${R_SCRIPT_DIR}  -v ${DOCKER_VOLUME}:${IMAGE_VOLUME_PATH} ${IMAGE_NAME} ${SCRIPT_NAME} ${DATE} ${RUN_DAY} ${CSV_FILES_DIR} ${BOOLEAN} ${HTML_FILE}"

docker run -i --name ${CONTAINER_NAME} --workdir ${R_SCRIPT_DIR}  -v ${DOCKER_VOLUME}:${IMAGE_VOLUME_PATH} ${IMAGE_NAME} ${SCRIPT_NAME} ${DATE} ${RUN_DAY} ${CSV_FILES_DIR} ${BOOLEAN} ${HTML_FILE}
