#!/bin/bash
##Create by Pavan Kumar
##These commnads run the alpine nginx to show the html file of locator R analysis

HOST_HTML_VOL=/bk3/locator_R_analysis/docker_volume/html
NGINX_CONF_FILE=/bk3/locator_R_analysis/docker_volume/conf

IMAGE_HTML_VOL=/usr/share/nginx/html
IMAGE_CONF_VOL=/etc/nginx/conf.d
IMAGE_NAME=nginx:1.15.4-alpine
PORTS=8080:80
CONTAINER_NAME=r-nginx

RUNNING_CONTAINER_NAME=$(docker ps -a |grep ${CONTAINER_NAME} |awk  '{print $NF}')

docker stop ${RUNNING_CONTAINER_NAME}

docker rm ${RUNNING_CONTAINER_NAME}



docker run -d -p ${PORTS} -v ${HOST_HTML_VOL}:${IMAGE_HTML_VOL} -v ${NGINX_CONF_FILE}:${IMAGE_CONF_VOL} --name ${CONTAINER_NAME} ${IMAGE_NAME}
