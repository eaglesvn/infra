library(rmarkdown)
library(dplyr)

source("locator-summary-functions.r")

options(width=80) 
args = commandArgs(trailingOnly=TRUE)
if (length(args) < 4) {
  stop("Arguments are:\nDate with format 'YYYY-MM-DD'\nSummary type: ('Daily', 'Weekly', 'Monthly').\nPath to the CSV files\nShould filter: true or false\n(Optional) Path to output directory, default is ./", call.=FALSE)
}

referenceDate <- args[1]
if (!grepl("\\d{4}-\\d{2}-\\d{2}", referenceDate)) {
  stop("Invalid date in first argument. Enter the date in the following format: YYYY-MM-DD")
}

reportType <- args[2]
if (!(reportType %in% c("Daily", "Weekly", "Monthly"))) {
  stop("Invalid report type in the second argument. Enter 'Daily', 'Weekly', or 'Monthly'")
}

pathToCsv <- args[3]
stringLength <- nchar(pathToCsv)
endingCharacter <- substr(pathToCsv,stringLength,stringLength)
if (!(endingCharacter %in% "/")) {
  stop("Invalid path to CSV directory. Path must end with a forward slash (/)");
}

shouldFilter <- args[4]
if(!(shouldFilter %in% c("true", "false"))) {
  stop("Invalid filter parameter, must be true of false");
}

outputPath <- "./"
if (length(args) == 5) {
    outputPath <- args[5]
    outputStringLength <- nchar(outputPath)
    endingCharacter <- substr(outputPath,outputStringLength,outputStringLength)
    if (!(endingCharacter %in% "/")) {
        stop("Invalid output path. Path must end with a forward slash (/)");
    }
}

# validated inputs, now perform file fetch based on report type
if (reportType %in% c("Daily")) {
  fileList <- c(getFilesEndingWithDate(referenceDate), getFilesStartingWithDate(referenceDate))
  outerLoopValue <- 1;
  dateToSearchFor = as.Date(referenceDate)
  endDate <- dateToSearchFor
} else if (reportType %in% c("Weekly")) {
  # monday = 1, tuesday = 2, ..., sunday = 7
  dayOfWeek <- strtoi(format(as.Date(referenceDate), "%u"))
  # get number of days in the week before this one
  daysBefore <- dayOfWeek - 1
  # start at the beginning of the week
  dateInWeek <- as.Date(referenceDate)-daysBefore
  # for each day, get the related files and append to file list
  fileList <- list()
  for(i in 1:7) {
    fileList <- c(fileList, getFilesEndingWithDate(dateInWeek), getFilesStartingWithDate(dateInWeek))
    dateInWeek <- dateInWeek + 1
  }
  outerLoopValue <- 7;
  dateToSearchFor <- as.Date(referenceDate) - daysBefore
  endDate <- dateInWeek - 1
} else if (reportType %in% c("Monthly")) {
  # get the day of the month
  dayOfMonth <- strtoi(format(as.Date(referenceDate), "%d"))
  # get the number of days before this one in the month
  daysBefore <- dayOfMonth - 1
  # start at beginning of month
  dateInMonth = as.Date(referenceDate)-daysBefore
  # get the number of days in the month
  daysInMonth <- daysInMonthForDate(dateInMonth)
  # for each day, get the related files and append to file list
  fileList <- list()
  for(i in 1:daysInMonth) {
    fileList <- c(fileList, getFilesStartingWithDate(dateInMonth), getFilesEndingWithDate(dateInMonth))
    dateInMonth <- dateInMonth + 1
  }
  outerLoopValue <- daysInMonth;
  dateToSearchFor = as.Date(referenceDate) - daysBefore
  endDate <- dateInMonth - 1
}

# Remove duplicate files
fileList <- unique(fileList)

# Put data into dataframe
datalist <- lapply(fileList, read.csv)

df <- do.call("rbind", datalist) 

if (is.null(df)) {
  stop("No data was found.");
}

if (shouldFilter %in% c("true")) {
  df <- df %>% filter(!grepl('.ico|.png|.css|.jpeg|.jpg|.js|.gif', VALUE))
}

df <- df %>% filter(as.Date(DATE) >= dateToSearchFor) %>% filter(as.Date(DATE) <= endDate) %>% group_by(DATE,HOUR,CATEGORY,VALUE) %>% summarise(HITS = sum(HITS)) %>% ungroup()

rmarkdown::render('locator-summary.rmd', output_dir = outputPath)
